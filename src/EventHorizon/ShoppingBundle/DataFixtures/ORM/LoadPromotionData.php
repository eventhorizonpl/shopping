<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\ShoppingBundle\Entity\Promotion;

class LoadPromotionData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $date = new \DateTime();
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$companys; $i++) {
                for ($j = 1; $j <= Conf::$companyusers; $j++) {
                    for ($k = 1; $k <= Conf::$promotions; $k++) {
                        $promotion = new Promotion();
                        $promotion->setCompany($manager->merge($this->getReference('company'.$i)));
                        $promotion->setUser($manager->merge($this->getReference('companyuser'.$j)));
                        $promotion->setDescription('description'.$i.'_'.$j.'_'.$k);
                        $promotion->setDisplayingEndsAt($date);
                        $promotion->setDisplayingStartsAt($date);
                        $promotion->setIsAccepted(true);
                        $promotion->setIsVisible(true);
                        $promotion->setPromotionEndsAt($date);
                        $promotion->setPromotionStartsAt($date);
                        $promotion->setUpperLimit($i);
                        $promotion->setTitle('title'.$i.'_'.$j.'_'.$k);
                        $promotion->setSlug('slug'.$i.'_'.$j.'_'.$k);
                        $promotion->setPercent($i);

                        $manager->persist($promotion);

                        $this->addReference('promotion'.$i.'_'.$j.'_'.$k, $promotion);
                    }
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 60;
    }
}
