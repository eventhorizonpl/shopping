<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

class Conf
{
    public static $all_fixtures = 1;
    public static $customers = 5;
    public static $companyusers = 5;
    public static $companys = 5;
    public static $contacts = 5;
    public static $promotions = 5;
}
