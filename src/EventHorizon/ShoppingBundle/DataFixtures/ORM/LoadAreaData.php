<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use EventHorizon\ShoppingBundle\Entity\Area;

class LoadAreaData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $dolnoslaskie = new Area();
        $dolnoslaskie->setName("dolnośląskie");
        $dolnoslaskie->setIsVisible(true);
        $manager->persist($dolnoslaskie);
        $this->addReference('dolnoslaskie', $dolnoslaskie);

        $legnica = new Area();
        $legnica->setName("Legnica");
        $legnica->setIsVisible(true);
        $legnica->setParent($dolnoslaskie);
        $manager->persist($legnica);
        $this->addReference('legnica', $legnica);

        $walbrzych = new Area();
        $walbrzych->setName("Wałbrzych");
        $walbrzych->setIsVisible(true);
        $walbrzych->setParent($dolnoslaskie);
        $manager->persist($walbrzych);
        $this->addReference('walbrzych', $walbrzych);

        $wroclaw = new Area();
        $wroclaw->setName("Wrocław");
        $wroclaw->setIsVisible(true);
        $wroclaw->setParent($dolnoslaskie);
        $manager->persist($wroclaw);
        $this->addReference('wroclaw', $wroclaw);

        $kujawskopomorskie = new Area();
        $kujawskopomorskie->setName("kujawsko-pomorskie");
        $kujawskopomorskie->setIsVisible(true);
        $manager->persist($kujawskopomorskie);
        $this->addReference('kujawskopomorskie', $kujawskopomorskie);

        $bydgoszcz = new Area();
        $bydgoszcz->setName("Bydgoszcz");
        $bydgoszcz->setIsVisible(true);
        $bydgoszcz->setParent($kujawskopomorskie);
        $manager->persist($bydgoszcz);
        $this->addReference('bydgoszcz', $bydgoszcz);

        $torun = new Area();
        $torun->setName("Toruń");
        $torun->setIsVisible(true);
        $torun->setParent($kujawskopomorskie);
        $manager->persist($torun);
        $this->addReference('torun', $torun);

        $wloclawek = new Area();
        $wloclawek->setName("Włocławek");
        $wloclawek->setIsVisible(true);
        $wloclawek->setParent($kujawskopomorskie);
        $manager->persist($wloclawek);
        $this->addReference('wloclawek', $wloclawek);

        $lubelskie = new Area();
        $lubelskie->setName("lubelskie");
        $lubelskie->setIsVisible(true);
        $manager->persist($lubelskie);
        $this->addReference('lubelskie', $lubelskie);

        $lubuskie = new Area();
        $lubuskie->setName("lubuskie");
        $lubuskie->setIsVisible(true);
        $manager->persist($lubuskie);
        $this->addReference('lubuskie', $lubuskie);

        $lublin = new Area();
        $lublin->setName("Lublin");
        $lublin->setIsVisible(true);
        $lublin->setParent($lubuskie);
        $manager->persist($lublin);
        $this->addReference('lublin', $lublin);

        $gorzowwielkopolski = new Area();
        $gorzowwielkopolski->setName("Gorzów Wielkopolski");
        $gorzowwielkopolski->setIsVisible(true);
        $gorzowwielkopolski->setParent($lubuskie);
        $manager->persist($gorzowwielkopolski);
        $this->addReference('gorzowwielkopolski', $gorzowwielkopolski);

        $zielonagora = new Area();
        $zielonagora->setName("Zielona Góra");
        $zielonagora->setIsVisible(true);
        $zielonagora->setParent($lubuskie);
        $manager->persist($zielonagora);
        $this->addReference('zielonagora', $zielonagora);

        $lodzkie = new Area();
        $lodzkie->setName("łódzkie");
        $lodzkie->setIsVisible(true);
        $manager->persist($lodzkie);
        $this->addReference('lodzkie', $lodzkie);

        $lodz = new Area();
        $lodz->setName("Łódź");
        $lodz->setIsVisible(true);
        $lodz->setParent($lodzkie);
        $manager->persist($lodz);
        $this->addReference('lodz', $lodz);

        $malopolskie = new Area();
        $malopolskie->setName("małopolskie");
        $malopolskie->setIsVisible(true);
        $manager->persist($malopolskie);
        $this->addReference('malopolskie', $malopolskie);

        $krakow = new Area();
        $krakow->setName("Kraków");
        $krakow->setIsVisible(true);
        $krakow->setParent($malopolskie);
        $manager->persist($krakow);
        $this->addReference('krakow', $krakow);

        $tarnow = new Area();
        $tarnow->setName("Tarnów");
        $tarnow->setIsVisible(true);
        $tarnow->setParent($malopolskie);
        $manager->persist($tarnow);
        $this->addReference('tarnow', $tarnow);

        $mazowieckie = new Area();
        $mazowieckie->setName("mazowieckie");
        $mazowieckie->setIsVisible(true);
        $manager->persist($mazowieckie);
        $this->addReference('mazowieckie', $mazowieckie);

        $plock = new Area();
        $plock->setName("Płock");
        $plock->setIsVisible(true);
        $plock->setParent($mazowieckie);
        $manager->persist($plock);
        $this->addReference('plock', $plock);

        $radom = new Area();
        $radom->setName("Radom");
        $radom->setIsVisible(true);
        $radom->setParent($mazowieckie);
        $manager->persist($radom);
        $this->addReference('radom', $radom);

        $warszawa = new Area();
        $warszawa->setName("Warszawa");
        $warszawa->setIsVisible(true);
        $warszawa->setParent($mazowieckie);
        $manager->persist($warszawa);
        $this->addReference('warszawa', $warszawa);

        $opolskie = new Area();
        $opolskie->setName("opolskie");
        $opolskie->setIsVisible(true);
        $manager->persist($opolskie);
        $this->addReference('opolskie', $opolskie);

        $opole = new Area();
        $opole->setName("Opole");
        $opole->setIsVisible(true);
        $opole->setParent($opolskie);
        $manager->persist($opole);
        $this->addReference('opole', $opole);

        $podkarpackie = new Area();
        $podkarpackie->setName("podkarpackie");
        $podkarpackie->setIsVisible(true);
        $manager->persist($podkarpackie);
        $this->addReference('podkarpackie', $podkarpackie);

        $rzeszow = new Area();
        $rzeszow->setName("Rzeszów");
        $rzeszow->setIsVisible(true);
        $rzeszow->setParent($podkarpackie);
        $manager->persist($rzeszow);
        $this->addReference('rzeszow', $rzeszow);

        $podlaskie = new Area();
        $podlaskie->setName("podlaskie");
        $podlaskie->setIsVisible(true);
        $manager->persist($podlaskie);
        $this->addReference('podlaskie', $podlaskie);

        $bialystok = new Area();
        $bialystok->setName("Białystok");
        $bialystok->setIsVisible(true);
        $bialystok->setParent($podlaskie);
        $manager->persist($bialystok);
        $this->addReference('bialystok', $bialystok);

        $pomorskie = new Area();
        $pomorskie->setName("pomorskie");
        $pomorskie->setIsVisible(true);
        $manager->persist($pomorskie);
        $this->addReference('pomorskie', $pomorskie);

        $gdansk = new Area();
        $gdansk->setName("Gdańsk");
        $gdansk->setIsVisible(true);
        $gdansk->setParent($pomorskie);
        $manager->persist($gdansk);
        $this->addReference('gdansk', $gdansk);

        $gdynia = new Area();
        $gdynia->setName("Gdynia");
        $gdynia->setIsVisible(true);
        $gdynia->setParent($pomorskie);
        $manager->persist($gdynia);
        $this->addReference('gdynia', $gdynia);

        $slaskie = new Area();
        $slaskie->setName("śląskie");
        $slaskie->setIsVisible(true);
        $manager->persist($slaskie);
        $this->addReference('slaskie', $slaskie);

        $bielskobiala = new Area();
        $bielskobiala->setName("Bielsko-Biała");
        $bielskobiala->setIsVisible(true);
        $bielskobiala->setParent($slaskie);
        $manager->persist($bielskobiala);
        $this->addReference('bielskobiala', $bielskobiala);

        $bytom = new Area();
        $bytom->setName("Bytom");
        $bytom->setIsVisible(true);
        $bytom->setParent($slaskie);
        $manager->persist($bytom);
        $this->addReference('bytom', $bytom);

        $chorzow = new Area();
        $chorzow->setName("Chorzów");
        $chorzow->setIsVisible(true);
        $chorzow->setParent($slaskie);
        $manager->persist($chorzow);
        $this->addReference('chorzow', $chorzow);

        $czestochowa = new Area();
        $czestochowa->setName("Częstochowa");
        $czestochowa->setIsVisible(true);
        $czestochowa->setParent($slaskie);
        $manager->persist($czestochowa);
        $this->addReference('czestochowa', $czestochowa);

        $dabrowagornicza = new Area();
        $dabrowagornicza->setName("Dąbrowa Górnicza");
        $dabrowagornicza->setIsVisible(true);
        $dabrowagornicza->setParent($slaskie);
        $manager->persist($dabrowagornicza);
        $this->addReference('dabrowagornicza', $dabrowagornicza);

        $gliwice = new Area();
        $gliwice->setName("Gliwice");
        $gliwice->setIsVisible(true);
        $gliwice->setParent($slaskie);
        $manager->persist($gliwice);
        $this->addReference('gliwice', $gliwice);

        $katowice = new Area();
        $katowice->setName("Katowice");
        $katowice->setIsVisible(true);
        $katowice->setParent($slaskie);
        $manager->persist($katowice);
        $this->addReference('katowice', $katowice);

        $rudaslaska = new Area();
        $rudaslaska->setName("Ruda Śląska");
        $rudaslaska->setIsVisible(true);
        $rudaslaska->setParent($slaskie);
        $manager->persist($rudaslaska);
        $this->addReference('rudaslaska', $rudaslaska);

        $rybnik = new Area();
        $rybnik->setName("Rybnik");
        $rybnik->setIsVisible(true);
        $rybnik->setParent($slaskie);
        $manager->persist($rybnik);
        $this->addReference('rybnik', $rybnik);

        $sosnowiec = new Area();
        $sosnowiec->setName("Sosnowiec");
        $sosnowiec->setIsVisible(true);
        $sosnowiec->setParent($slaskie);
        $manager->persist($sosnowiec);
        $this->addReference('sosnowiec', $sosnowiec);

        $tychy = new Area();
        $tychy->setName("Tychy");
        $tychy->setIsVisible(true);
        $tychy->setParent($slaskie);
        $manager->persist($tychy);
        $this->addReference('tychy', $tychy);

        $zabrze = new Area();
        $zabrze->setName("Zabrze");
        $zabrze->setIsVisible(true);
        $zabrze->setParent($slaskie);
        $manager->persist($zabrze);
        $this->addReference('zabrze', $zabrze);

        $swietokrzyskie = new Area();
        $swietokrzyskie->setName("świętokrzyskie");
        $swietokrzyskie->setIsVisible(true);
        $manager->persist($swietokrzyskie);
        $this->addReference('swietokrzyskie', $swietokrzyskie);

        $kielce = new Area();
        $kielce->setName("Kielce");
        $kielce->setIsVisible(true);
        $kielce->setParent($swietokrzyskie);
        $manager->persist($kielce);
        $this->addReference('kielce', $kielce);

        $warminskomazurskie = new Area();
        $warminskomazurskie->setName("warmińsko-mazurskie");
        $warminskomazurskie->setIsVisible(true);
        $manager->persist($warminskomazurskie);
        $this->addReference('warminskomazurskie', $warminskomazurskie);

        $elblag = new Area();
        $elblag->setName("Elbląg");
        $elblag->setIsVisible(true);
        $elblag->setParent($warminskomazurskie);
        $manager->persist($elblag);
        $this->addReference('elblag', $elblag);

        $olsztyn = new Area();
        $olsztyn->setName("Olsztyn");
        $olsztyn->setIsVisible(true);
        $olsztyn->setParent($warminskomazurskie);
        $manager->persist($olsztyn);
        $this->addReference('olsztyn', $olsztyn);

        $wielkopolskie = new Area();
        $wielkopolskie->setName("wielkopolskie");
        $wielkopolskie->setIsVisible(true);
        $manager->persist($wielkopolskie);
        $this->addReference('wielkopolskie', $wielkopolskie);

        $kalisz = new Area();
        $kalisz->setName("Kalisz");
        $kalisz->setIsVisible(true);
        $kalisz->setParent($wielkopolskie);
        $manager->persist($kalisz);
        $this->addReference('kalisz', $kalisz);

        $poznan = new Area();
        $poznan->setName("Poznań");
        $poznan->setIsVisible(true);
        $poznan->setParent($wielkopolskie);
        $manager->persist($poznan);
        $this->addReference('poznan', $poznan);

        $zachodniopomorskie = new Area();
        $zachodniopomorskie->setName("zachodniopomorskie");
        $zachodniopomorskie->setIsVisible(true);
        $manager->persist($zachodniopomorskie);
        $this->addReference('zachodniopomorskie', $zachodniopomorskie);

        $koszalin = new Area();
        $koszalin->setName("Koszalin");
        $koszalin->setIsVisible(true);
        $koszalin->setParent($zachodniopomorskie);
        $manager->persist($koszalin);
        $this->addReference('koszalin', $koszalin);

        $szczecin = new Area();
        $szczecin->setName("Szczecin");
        $szczecin->setIsVisible(true);
        $szczecin->setParent($zachodniopomorskie);
        $manager->persist($szczecin);
        $this->addReference('szczecin', $szczecin);

        $manager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}
