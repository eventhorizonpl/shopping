<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\ShoppingBundle\Entity\Contact;

class LoadContactData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$companys; $i++) {
                for ($j = 1; $j <= Conf::$contacts; $j++) {
                    $contact = new Contact();
                    $contact->setCompany($manager->merge($this->getReference('company'.$i)));
                    $contact->setCity('city'.$i.'_'.$j);
                    $contact->setCountry('country'.$i.'_'.$j);
                    $contact->setEmail('email'.$i.'_'.$j);
                    $contact->setStreet('street'.$i.'_'.$j);
                    $contact->setZipCode('zip code'.$i.'_'.$j);
                    $manager->persist($contact);

                    $this->addReference('contact'.$i.'_'.$j, $contact);
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 21;
    }
}
