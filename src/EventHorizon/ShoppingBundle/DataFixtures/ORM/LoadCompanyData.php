<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\ShoppingBundle\Entity\Company;

class LoadCompanyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$companys; $i++) {
                $company = new Company();
                $company->setUser($manager->merge($this->getReference('companyuser'.$i)));
                $company->setBankAccount('bank account'.$i);
                $company->setDescription('description'.$i);
                $company->setName('name'.$i);
                $company->setNip('nip'.$i);
                $company->setWww('www'.$i);
                $company->setIsBlocked(false);
                $company->setIsVerified(true);

                $manager->persist($company);
                $this->addReference('company'.$i, $company);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
