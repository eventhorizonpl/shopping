<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\SecurityBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName('admin');
        $user->setLastName('admin');
        $user->setEmail('admin@admin.com');
        $user->setEnabled(true);
        $user->setPlainPassword('admin');
        $user->setRoles(array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER'));
        $user->setSuperAdmin(true);
        $user->setUsername('admin');

        $manager->persist($user);

        $this->addReference('admin-user', $user);

        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$customers; $i++) {
                $user = new User();
                $user->setFirstName('customer'.$i);
                $user->setLastName('customer'.$i);
                $user->setEmail('customer'.$i.'@customer'.$i.'.com');
                $user->setEnabled(true);
                $user->setPlainPassword('customer'.$i);
                $user->setRoles(array('ROLE_CUSTOMER_USER', 'ROLE_USER'));
                $user->setUsername('customer'.$i);

                $manager->persist($user);
                $this->addReference('customeruser'.$i, $user);
            }

            for ($i = 1; $i <= Conf::$companyusers; $i++) {
                $user = new User();
                $user->setFirstName('companyuser'.$i);
                $user->setLastName('companyuser'.$i);
                $user->setEmail('companyuser'.$i.'@companyuser'.$i.'.com');
                $user->setEnabled(true);
                $user->setPlainPassword('companyuser'.$i);
                $user->setRoles(array('ROLE_COMPANY_ACCOUNT_MANAGER', 'ROLE_COMPANY_DIRECTOR', 'ROLE_COMPANY_USER', 'ROLE_HUMAN_RESOURCE_MANAGER', 'ROLE_PROMOTION_MANAGER', 'ROLE_USER'));
                $user->setUsername('companyuser'.$i);

                $manager->persist($user);
                $this->addReference('companyuser'.$i, $user);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
