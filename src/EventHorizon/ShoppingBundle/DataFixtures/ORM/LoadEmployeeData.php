<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\ShoppingBundle\Entity\Employee;

class LoadEmployeeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$companyusers; $i++) {
                $employee = new Employee();
                $employee->setCompany($manager->merge($this->getReference('company'.$i)));
                $employee->setUser($manager->merge($this->getReference('companyuser'.$i)));
                $employee->setIsBlocked(false);
                $employee->setIsCompanyDirector(true);
                $manager->persist($employee);

                $this->addReference('employee'.$i, $employee);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 30;
    }
}
