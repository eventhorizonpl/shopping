<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use EventHorizon\ShoppingBundle\Entity\Trade;

class LoadTradeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $artykulyspozywcze = new Trade();
        $artykulyspozywcze->setName("Artykuły spożywcze");
        $artykulyspozywcze->setIsVisible(true);
        $manager->persist($artykulyspozywcze);
        $this->addReference('artykulyspozywcze', $artykulyspozywcze);

        $domiogrod = new Trade();
        $domiogrod->setName("Dom i ogród");
        $domiogrod->setIsVisible(true);
        $manager->persist($domiogrod);
        $this->addReference('domiogrod', $domiogrod);

        $manager->flush();
    }

    public function getOrder()
    {
        return 41;
    }
}
