<?php

namespace EventHorizon\ShoppingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\ShoppingBundle\DataFixtures\ORM\Conf;
use EventHorizon\ShoppingBundle\Entity\InvoiceAddress;

class LoadInvoiceAddressData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$companys; $i++) {
                $invoiceAddress = new InvoiceAddress();
                $invoiceAddress->setCompany($manager->merge($this->getReference('company'.$i)));
                $invoiceAddress->setCity('city'.$i);
                $invoiceAddress->setCountry('country'.$i);
                $invoiceAddress->setEmail('email'.$i);
                $invoiceAddress->setStreet('street'.$i);
                $invoiceAddress->setZipCode('zip code'.$i);
                $manager->persist($invoiceAddress);

                $this->addReference('invoice_address'.$i, $invoiceAddress);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 32;
    }
}
