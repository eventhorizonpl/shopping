<?php

namespace EventHorizon\ShoppingBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Contact;
use EventHorizon\ShoppingBundle\Entity\InvoiceAddress;
use EventHorizon\ShoppingBundle\Form\CompanyType;

/**
 * Account controller.
 *
 * @Route("/account")
 */
class AccountController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @DI\Inject("security.context")
     */
    private $security;

    /**
     * @param Company $company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    private function bindContact($company)
    {
        if (0 === count($company->getContacts())) {
            $contact = new Contact();
            $contact->setCompany($company);
            $company->addContact($contact);
        }

        return $company;
    }

    /**
     * @param Company $company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    private function bindInvoiceAddress($company)
    {
        if (null === $company->getInvoiceAddress()) {
            $invoiceAddress = new InvoiceAddress();
            $invoiceAddress->setCompany($company);
            $company->setInvoiceAddress($invoiceAddress);
        }

        return $company;
    }

    /**
     * @param Company $company
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Company $company)
    {
        $form = $this->createForm(new CompanyType(), $company, array(
            'action' => $this->generateUrl('account_update'),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
    * @return \EventHorizon\ShoppingBundle\Entity\Company
    */
    private function getCompanyObject()
    {
        $user = $this->security->getToken()->getUser();
        $employee = $this->em->getRepository('EventHorizonShoppingBundle:Employee')->getEmployeeByUserV1($user);
        if (!($employee)) {
            throw $this->createNotFoundException('Unable to find Employee entity.');
        }

        $company = $employee->getCompany();
        $company = $this->em->getRepository('EventHorizonShoppingBundle:Company')->getCompanyByIdV1($company);

        if (!($company)) {
            throw $this->createNotFoundException('Unable to find Company entity.');
        }

        return $company;
    }

    /**
     * @Method("GET")
     * @Route("/", name="account_show")
     * @Secure(roles="ROLE_COMPANY_USER")
     * @Template("EventHorizonShoppingBundle:Account:show.html.twig")
     */
    public function showAction()
    {
        $company = $this->getCompanyObject();

        return array(
            'company' => $company,
        );
    }

    /**
     * @Method("GET")
     * @Route("/edit", name="account_edit")
     * @Secure(roles="ROLE_COMPANY_ACCOUNT_MANAGER, ROLE_COMPANY_DIRECTOR")
     * @Template("EventHorizonShoppingBundle:Account:edit.html.twig")
     */
    public function editAction()
    {
        $company = $this->getCompanyObject();
        $company = $this->bindContact($company);
        $company = $this->bindInvoiceAddress($company);

        $editForm = $this->createEditForm($company);

        return array(
            'company' => $company,
            'form'    => $editForm->createView(),
        );
    }

    /**
     * @Method("PUT")
     * @Route("/edit", name="account_update")
     * @Secure(roles="ROLE_COMPANY_ACCOUNT_MANAGER, ROLE_COMPANY_DIRECTOR")
     * @Template("EventHorizonShoppingBundle:Account:edit.html.twig")
     */
    public function updateAction(Request $request)
    {
        $company = $this->getCompanyObject();
        $company = $this->bindContact($company);
        $company = $this->bindInvoiceAddress($company);

        $editForm = $this->createEditForm($company);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->em->flush();

            return $this->redirect($this->generateUrl('account_show'));
        }

        return array(
            'company' => $company,
            'form'    => $editForm->createView(),
        );
    }
}
