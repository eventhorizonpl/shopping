<?php

namespace EventHorizon\ShoppingBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\ShoppingBundle\Entity\Promotion;
use EventHorizon\ShoppingBundle\Form\PromotionType;

/**
 * Promotion controller.
 *
 * @Route("/promotion")
 */
class PromotionController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @DI\Inject("security.context")
     */
    private $security;

    /**
    * Creates a form to create a Promotion entity.
    *
    * @param Promotion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Promotion $entity)
    {
        $form = $this->createForm(new PromotionType(), $entity, array(
            'action' => $this->generateUrl('promotion_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
    * Creates a form to edit a Promotion entity.
    *
    * @param Promotion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Promotion $entity)
    {
        $form = $this->createForm(new PromotionType(), $entity, array(
            'action' => $this->generateUrl('promotion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * @return \EventHorizon\ShoppingBundle\Entity\Employee
     */
    private function getEmployeeObject()
    {
        $user = $this->security->getToken()->getUser();
        $employee = $this->em->getRepository('EventHorizonShoppingBundle:Employee')->getEmployeeByUserV1($user);
        if (!($employee)) {
            throw $this->createNotFoundException('Unable to find Employee entity.');
        }

        return $employee;
    }

    /**
     * @return \EventHorizon\ShoppingBundle\Entity\Promotion
     */
    private function getPromotionObject($promotionId)
    {
        $employee = $this->getEmployeeObject();
        $company = $employee->getCompany();

        $promotion = $this->em->getRepository('EventHorizonShoppingBundle:Promotion')->getPromotionsByIdAndCompanyV1($promotionId, $company);

        if (!($promotion)) {
            $this->message = "EventHorizonShoppingBundle:Promotion:getPromotionObject id=".$promotionId." userId=".$this->security->getToken()->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Promotion entity.');
        }

        return $promotion;
    }

    /**
     * Lists all Promotion entities.
     *
     * @Method("GET")
     * @Route("/", name="promotion_index")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_PROMOTION_MANAGER")
     * @Template("EventHorizonShoppingBundle:Promotion:index.html.twig")
     */
    public function indexAction()
    {
        $employee = $this->getEmployeeObject();
        $company = $employee->getCompany();

        $promotions = $this->em->getRepository('EventHorizonShoppingBundle:Promotion')->getPromotionsByCompanyV1($company);

        return array(
            'promotions' => $promotions,
        );
    }

    /**
     * Displays a form to create a new Promotion entity.
     *
     * @Method("GET")
     * @Route("/new", name="promotion_new")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_PROMOTION_MANAGER")
     * @Template("EventHorizonShoppingBundle:Promotion:new.html.twig")
     */
    public function newAction()
    {
        $promotion = new Promotion();
        $form = $this->createCreateForm($promotion);

        return array(
            'form'      => $form->createView(),
            'promotion' => $promotion,
        );
    }

    /**
     * Creates a new Promotion entity.
     *
     * @Method("POST")
     * @Route("/new", name="promotion_create")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_PROMOTION_MANAGER")
     * @Template("EventHorizonShoppingBundle:Promotion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $promotion = new Promotion();
        $form = $this->createCreateForm($promotion);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $employee = $this->getEmployeeObject();
            $company = $employee->getCompany();
            $user = $employee->getUser();
            $promotion = $this->em->getRepository('EventHorizonShoppingBundle:Promotion')->saveNewPromotion($company, $promotion, $user);

            return $this->redirect($this->generateUrl('promotion_index'));
        }

        return array(
            'form'      => $form->createView(),
            'promotion' => $promotion,
        );
    }

    /**
     * Displays a form to edit an existing Promotion entity.
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="promotion_edit", requirements={"id" = "\d+"})
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_PROMOTION_MANAGER")
     * @Template("EventHorizonShoppingBundle:Promotion:edit.html.twig")
     */
    public function editAction($id)
    {
        $promotion = $this->getPromotionObject($id);

        $editForm = $this->createEditForm($promotion);

        return array(
            'form'      => $editForm->createView(),
            'promotion' => $promotion,
        );
    }

    /**
     * Edits an existing Promotion entity.
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="promotion_update", requirements={"id" = "\d+"})
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_PROMOTION_MANAGER")
     * @Template("EventHorizonShoppingBundle:Promotion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $promotion = $this->getPromotionObject($id);

        $editForm = $this->createEditForm($promotion);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->em->flush();

            return $this->redirect($this->generateUrl('promotion_index'));
        }

        return array(
            'form'      => $editForm->createView(),
            'promotion' => $promotion,
        );
    }
}
