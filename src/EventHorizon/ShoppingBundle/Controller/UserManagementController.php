<?php

namespace EventHorizon\ShoppingBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Form\UserType;

/**
 * UserManagement controller.
 *
 * @Route("/user_management")
 */
class UserManagementController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @DI\Inject("security.context")
     */
    private $security;

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_management_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_management_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * @return \EventHorizon\ShoppingBundle\Entity\Employee
     */
    private function getEmployeeObject()
    {
        $user = $this->security->getToken()->getUser();
        $employee = $this->em->getRepository('EventHorizonShoppingBundle:Employee')->getEmployeeByUserV1($user);
        if (!($employee)) {
            throw $this->createNotFoundException('Unable to find Employee entity.');
        }

        return $employee;
    }

    /**
     * @return \EventHorizon\SecurityBundle\Entity\User
     */
    private function getUserObject($userId)
    {
        $employee = $this->getEmployeeObject();

        $company = $employee->getCompany();

        $employee = $this->em->getRepository('EventHorizonShoppingBundle:Employee')->getEmployeeByCompanyAndUserIdV1($company, $userId);

        if (!($employee)) {
            $this->message = "EventHorizonShoppingBundle:UserManagement:getUserObject id=".$userId." userId=".$this->security->getToken()->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Employee entity.');
        }

        $user = $employee->getUser();

        return $user;
    }

    /**
     * Lists all User entities.
     *
     * @Method("GET")
     * @Route("/", name="user_management_index")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_HUMAN_RESOURCE_MANAGER")
     * @Template("EventHorizonShoppingBundle:UserManagement:index.html.twig")
     */
    public function indexAction()
    {
        $employee = $this->getEmployeeObject();
        $company = $employee->getCompany();

        $employees = $this->em->getRepository('EventHorizonShoppingBundle:Employee')->getEmployeesByCompanyV1($company);

        return array(
            'employees' => $employees,
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Method("GET")
     * @Route("/new", name="user_management_new")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_HUMAN_RESOURCE_MANAGER")
     * @Template("EventHorizonShoppingBundle:UserManagement:new.html.twig")
     */
    public function newAction()
    {
        $user = new User();
        $form = $this->createCreateForm($user);

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Method("POST")
     * @Route("/new", name="user_management_create")
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_HUMAN_RESOURCE_MANAGER")
     * @Template("EventHorizonShoppingBundle:UserManagement:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $employee = $this->getEmployeeObject();
        $company = $employee->getCompany();
        $form = $this->createCreateForm($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->getRepository('EventHorizonSecurityBundle:User')->saveUserManagementNew($company, $user);

            return $this->redirect($this->generateUrl('user_management_index'));
        }

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="user_management_edit", requirements={"id" = "\d+"})
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_HUMAN_RESOURCE_MANAGER")
     * @Template("EventHorizonShoppingBundle:UserManagement:edit.html.twig")
     */
    public function editAction($id)
    {
        $user = $this->getUserObject($id);

        $editForm = $this->createEditForm($user);

        return array(
            'form' => $editForm->createView(),
            'user' => $user,
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="user_management_update", requirements={"id" = "\d+"})
     * @Secure(roles="ROLE_COMPANY_DIRECTOR, ROLE_HUMAN_RESOURCE_MANAGER")
     * @Template("EventHorizonShoppingBundle:UserManagement:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->getUserObject($id);

        $editForm = $this->createEditForm($user);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->em->flush();

            return $this->redirect($this->generateUrl('user_management_index'));
        }

        return array(
            'form' => $editForm->createView(),
            'user' => $user,
        );
    }
}
