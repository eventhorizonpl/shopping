<?php

namespace EventHorizon\ShoppingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/", name="_homepage")
     * @Template("EventHorizonShoppingBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
        return array();
    }
}
