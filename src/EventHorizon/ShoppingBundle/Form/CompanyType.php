<?php

namespace EventHorizon\ShoppingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use EventHorizon\ShoppingBundle\Form\ContactType;
use EventHorizon\ShoppingBundle\Form\InvoiceAddressType;

class CompanyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bankAccount')
            ->add('contacts', 'collection', array(
                'allow_add' => true,
                'type'      => new ContactType()
            ))
            ->add('description')
            ->add('name', 'text', array(
                'label' => 'Nazwa firmy',
            ))
            ->add('invoiceAddress', new InvoiceAddressType(), array(
                'cascade_validation' => true
            ))
            ->add('nip')
            ->add('www')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\ShoppingBundle\Entity\Company'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventhorizon_shoppingbundle_company';
    }
}
