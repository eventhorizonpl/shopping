<?php

namespace EventHorizon\ShoppingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('enabled', 'checkbox', array(
                'required' => false,
            ))
            ->add('firstName')
            ->add('lastName')
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('roles', 'choice', array(
                'choices'  => array(
                    'ROLE_COMPANY_ACCOUNT_MANAGER' => 'Menedżer kont',
                    'ROLE_COMPANY_DIRECTOR' => 'Dyrektor firmy',
                    'ROLE_HUMAN_RESOURCE_MANAGER' => 'Menedżer HR',
                    'ROLE_PROMOTION_MANAGER' => 'Menedżer promocji',
                ),
                'multiple' => true,
                'required' => false,
            ))
            ->add('username')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\SecurityBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eventhorizon_securitybundle_user';
    }
}
