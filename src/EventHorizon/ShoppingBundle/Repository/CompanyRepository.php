<?php

namespace EventHorizon\ShoppingBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CompanyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CompanyRepository extends EntityRepository
{
    public function CompanySelectByIdV1($id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c, cc, cia FROM EventHorizonShoppingBundle:Company c LEFT JOIN c.contacts cc LEFT JOIN c.invoiceAddress cia WHERE c.id = :id')
            ->setParameter('id', $id);

        return $query;
    }

    public function getCompanyByIdV1($id)
    {
        $query = $this->CompanySelectByIdV1($id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
