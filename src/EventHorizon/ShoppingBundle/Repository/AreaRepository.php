<?php

namespace EventHorizon\ShoppingBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * AreaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AreaRepository extends NestedTreeRepository
{
}
