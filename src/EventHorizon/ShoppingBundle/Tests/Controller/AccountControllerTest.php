<?php

namespace EventHorizon\ShoppingBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccountControllerTest extends WebTestCase
{
    public function testShow()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/account/');
        $this->assertTrue($crawler->filter('html:contains("Dane firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane adresowe do faktur")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Www")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("NIP")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Konto bankowe")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Ulica")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Miasto")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kod pocztowy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kraj")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Email")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane kontaktowe")')->count() > 0);
    }

    public function testEdit()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/account/edit');
        $this->assertTrue($crawler->filter('html:contains("Dane firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane adresowe do faktur")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Strona www")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("NIP")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Konto bankowe")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Ulica")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Miasto")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kod pocztowy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kraj")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane kontaktowe")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();
        $crawler = $client->submit($form);
    }

    public function testEditNewCompany()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register/company');

        $this->assertTrue($crawler->filter('html:contains("Rejestracja")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("NIP")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);

        $form = $crawler->selectButton('Zarejestruj')->form();

        $date = date("Ymdhis");
        $form['fos_user_registration_form[username]'] = 'companyuser'.$date;
        $form['fos_user_registration_form[email]'] = 'companyusertest'.$date.'@companyusertest'.$date.'.com';
        $form['fos_user_registration_form[plainPassword][first]'] = 'companyusertest'.$date;
        $form['fos_user_registration_form[plainPassword][second]'] = 'companyusertest'.$date;
        $form['fos_user_registration_form[firstName]'] = 'companyusertest'.$date;
        $form['fos_user_registration_form[lastName]'] = 'companyusertest'.$date;
        $form['eventhorizon_securitybundle_RegistrationCompanyType[name]'] = 'companyusertest'.$date;
        $form['eventhorizon_securitybundle_RegistrationCompanyType[nip]'] = 'companyusertest'.$date;

        $crawler = $client->submit($form);

        $this->assertTrue($crawler->filter('html:contains("The user has been created successfully")')->count() > 0);

        $crawler = $client->request('GET', '/account/edit');
        $this->assertTrue($crawler->filter('html:contains("Dane firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane adresowe do faktur")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Strona www")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("NIP")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Konto bankowe")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Ulica")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Miasto")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kod pocztowy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Kraj")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dane kontaktowe")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();
        $crawler = $client->submit($form);
    }
}
