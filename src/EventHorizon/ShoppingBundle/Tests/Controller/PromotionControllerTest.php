<?php

namespace EventHorizon\ShoppingBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PromotionControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/promotion/');
        $this->assertTrue($crawler->filter('html:contains("Moje promocje")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Procent")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Limit")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Widoczność")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Akceptacja ze strony portalu")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie kończy się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja kończy się")')->count() > 0);
    }

    public function testNew()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/promotion/new');
        $this->assertTrue($crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Procent")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Limit")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Widoczność")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie kończy się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja kończy się")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_shoppingbundle_promotion[title]'] = 'test title';
        $form['eventhorizon_shoppingbundle_promotion[description]'] = 'test description';

        $crawler = $client->submit($form);

        $form['eventhorizon_shoppingbundle_promotion[percent]'] = '1';
        $form['eventhorizon_shoppingbundle_promotion[upperLimit]'] = '2';

        $crawler = $client->submit($form);
        $this->assertTrue($crawler->filter('html:contains("Moje promocje")')->count() > 0);
    }

    public function testEdit()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/promotion/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Procent")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Limit")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Widoczność")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Wyświetlanie kończy się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja zaczyna się")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promocja kończy się")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();

        $crawler = $client->submit($form);

        $form['eventhorizon_shoppingbundle_promotion[title]'] = 'test title 1';
        $form['eventhorizon_shoppingbundle_promotion[description]'] = 'test description 1';

        $crawler = $client->submit($form);
    }
}
