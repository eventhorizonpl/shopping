<?php

namespace EventHorizon\ShoppingBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/user_management/');
        $this->assertTrue($crawler->filter('html:contains("Zarządzanie użytkownikami")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko i imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Aktywność konta")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Uprawnienia")')->count() > 0);
    }

    public function testNew()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/user_management/new');
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Uprawnienia")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Konto włączone")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_securitybundle_user[firstName]'] = 'companyuser123';
        $form['eventhorizon_securitybundle_user[lastName]'] = 'companyuser123';

        $crawler = $client->submit($form);

        $form['eventhorizon_securitybundle_user[username]'] = 'companyuser123';
        $form['eventhorizon_securitybundle_user[email]'] = 'companyuser123@companyuser123.pl';
        $form['eventhorizon_securitybundle_user[plainPassword][first]'] = 'companyuser123';
        $form['eventhorizon_securitybundle_user[plainPassword][second]'] = 'companyuser123';

        $crawler = $client->submit($form);
        $this->assertTrue($crawler->filter('html:contains("Zarządzanie użytkownikami")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Aktywność konta")')->count() > 0);
    }

    public function testEdit()
    {
        $client = static::createClient();

        $client->followRedirects(true);

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'companyuser1';
        $form['_password'] = 'companyuser1';

        $crawler = $client->submit($form);

        $crawler = $client->request('GET', '/user_management/7/edit');
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Uprawnienia")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Konto włączone")')->count() > 0);

        $form = $crawler->selectButton('Zapisz')->form();

        $crawler = $client->submit($form);

        $form['eventhorizon_securitybundle_user[plainPassword][first]'] = 'companyuser1';
        $form['eventhorizon_securitybundle_user[plainPassword][second]'] = 'companyuser1';

        $crawler = $client->submit($form);
    }
}
