<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Contact;
use EventHorizon\ShoppingBundle\Entity\Employee;
use EventHorizon\ShoppingBundle\Entity\InvoiceAddress;
use EventHorizon\ShoppingBundle\Entity\Promotion;

class CompanyTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $bankAccount = "test bank account";
        $description = "test description";
        $name = "test name";
        $nip = "test nip";
        $www = "test www";
        $isBlocked = false;
        $isVerified = true;
        $user1 = "test user 1";
        $user2 = "test user 2";

        $contact = new Contact();
        $employee = new Employee();
        $invoiceAddress = new InvoiceAddress();
        $user = new User();

        $promotion = new Promotion();

        $company = new Company();
        $company->addContact($contact);
        $company->addEmployee($employee);
        $company->addPromotion($promotion);
        $company->setUser($user);
        $company->setInvoiceAddress($invoiceAddress);
        $company->setBankAccount($bankAccount);
        $company->setDescription($description);
        $company->setName($name);
        $company->setNip($nip);
        $company->setWww($www);
        $company->setIsBlocked($isBlocked);
        $company->setIsVerified($isVerified);
        $company->setCreatedBy($user1);
        $company->setUpdatedBy($user2);
        $company->setDeletedAt($date);
        $company->setCreatedAt($date);
        $company->setUpdatedAt($date);

        $this->assertNull($company->getId());
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $company->getContacts());
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $company->getEmployees());
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $company->getPromotions());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\InvoiceAddress', $company->getInvoiceAddress());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Entity\User', $company->getUser());
        $this->assertCount(1, $company->getContacts());
        $company->removeContact($contact);
        $this->assertCount(0, $company->getContacts());
        $this->assertCount(1, $company->getEmployees());
        $company->removeEmployee($employee);
        $this->assertCount(0, $company->getEmployees());

        $this->assertCount(1, $company->getPromotions());
        $company->removePromotion($promotion);
        $this->assertCount(0, $company->getPromotions());

        $this->assertEquals($bankAccount, $company->getBankAccount());
        $this->assertEquals($description, $company->getDescription());
        $this->assertEquals($name, $company->getName());
        $this->assertEquals($nip, $company->getNip());
        $this->assertEquals($www, $company->getWww());
        $this->assertEquals($isBlocked, $company->getIsBlocked());
        $this->assertEquals($isVerified, $company->getIsVerified());
        $this->assertEquals($user1, $company->getCreatedBy());
        $this->assertEquals($user2, $company->getUpdatedBy());
        $this->assertEquals($date, $company->getDeletedAt());
        $this->assertEquals($date, $company->getCreatedAt());
        $this->assertEquals($date, $company->getUpdatedAt());
    }

    public function testToString()
    {
        $name = "test name";

        $company = new Company();
        $company->setName($name);

        $this->assertEquals($name, $company->__toString());
    }
}
