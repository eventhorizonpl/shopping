<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\ShoppingBundle\Entity\Trade;

class TradeTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $lft = 1;
        $lvl = 2;
        $name = "test name";
        $parent = new Trade();
        $rgt = 3;
        $root = 4;
        $isVisible = true;
        $user1 = "test user 1";
        $user2 = "test user 2";

        $trade = new Trade();
        $trade->setLft($lft);
        $trade->setLvl($lvl);
        $trade->setName($name);
        $trade->setParent($parent);
        $trade->setRgt($rgt);
        $trade->setRoot($root);
        $trade->setIsVisible($isVisible);
        $trade->setCreatedBy($user1);
        $trade->setUpdatedBy($user2);
        $trade->setDeletedAt($date);
        $trade->setCreatedAt($date);
        $trade->setUpdatedAt($date);

        $this->assertNull($trade->getId());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Trade', $trade->getParent());
        $trade->setParentId($parent);
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Trade', $trade->getParentId());
        $trade->setParent(null);
        $this->assertNull($trade->getParent());
        $trade->setParentId(null);
        $this->assertNull($trade->getParentId());
        $this->assertEquals($lft, $trade->getLft());
        $this->assertEquals($lvl, $trade->getLvl());
        $this->assertEquals($name, $trade->getName());
        $this->assertEquals($rgt, $trade->getRgt());
        $this->assertEquals($root, $trade->getRoot());
        $this->assertEquals($isVisible, $trade->getIsVisible());
        $this->assertEquals($user1, $trade->getCreatedBy());
        $this->assertEquals($user2, $trade->getUpdatedBy());
        $this->assertEquals($date, $trade->getDeletedAt());
        $this->assertEquals($date, $trade->getCreatedAt());
        $this->assertEquals($date, $trade->getUpdatedAt());
    }

    public function testToString()
    {
        $name = "test name";

        $trade = new Trade();
        $trade->setName($name);

        $this->assertEquals($name, $trade->__toString());
    }
}
