<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Promotion;

class PromotionTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $description = "test description";
        $isAccepted = true;
        $percent = 1.1;
        $slug = "test-title";
        $title = "test title";
        $upperLimit = 2;
        $user1 = "test user 1";
        $user2 = "test user 2";
        $isVisible = true;

        $company = new Company();
        $user = new User();

        $promotion = new Promotion();
        $promotion->setCompany($company);
        $promotion->setUser($user);
        $promotion->setDescription($description);
        $promotion->setDisplayingEndsAt($date);
        $promotion->setDisplayingStartsAt($date);
        $promotion->setIsAccepted($isAccepted);
        $promotion->setPercent($percent);
        $promotion->setPromotionEndsAt($date);
        $promotion->setPromotionStartsAt($date);
        $promotion->setSlug($slug);
        $promotion->setTitle($title);
        $promotion->setUpperLimit($upperLimit);
        $promotion->setIsVisible($isVisible);
        $promotion->setCreatedBy($user1);
        $promotion->setUpdatedBy($user2);
        $promotion->setDeletedAt($date);
        $promotion->setCreatedAt($date);
        $promotion->setUpdatedAt($date);

        $this->assertNull($promotion->getId());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Company', $promotion->getCompany());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Entity\User', $promotion->getUser());
        $this->assertEquals($description, $promotion->getDescription());
        $this->assertEquals($date, $promotion->getDisplayingEndsAt());
        $this->assertEquals($date, $promotion->getDisplayingStartsAt());
        $this->assertEquals($isAccepted, $promotion->getIsAccepted());
        $this->assertEquals($percent, $promotion->getPercent());
        $this->assertEquals($date, $promotion->getPromotionEndsAt());
        $this->assertEquals($date, $promotion->getPromotionStartsAt());
        $this->assertEquals($slug, $promotion->getSlug());
        $this->assertEquals($title, $promotion->getTitle());
        $this->assertEquals($upperLimit, $promotion->getUpperLimit());
        $this->assertEquals($isVisible, $promotion->getIsVisible());
        $this->assertEquals($user1, $promotion->getCreatedBy());
        $this->assertEquals($user2, $promotion->getUpdatedBy());
        $this->assertEquals($date, $promotion->getDeletedAt());
        $this->assertEquals($date, $promotion->getCreatedAt());
        $this->assertEquals($date, $promotion->getUpdatedAt());
    }
}
