<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\InvoiceAddress;

class InvoiceAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $company = new Company();

        $invoiceAddress = new InvoiceAddress();
        $invoiceAddress->setCompany($company);

        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Company', $invoiceAddress->getCompany());
    }
}
