<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\ShoppingBundle\Entity\BaseAddress;

class BaseAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $city = "test city";
        $country = "test country";
        $email = "test email";
        $street = "test street";
        $zipCode = "test zip code";
        $user1 = "test user 1";
        $user2 = "test user 2";

        $baseAddress = new BaseAddress();
        $baseAddress->setCity($city);
        $baseAddress->setCountry($country);
        $baseAddress->setEmail($email);
        $baseAddress->setStreet($street);
        $baseAddress->setZipCode($zipCode);
        $baseAddress->setCreatedBy($user1);
        $baseAddress->setUpdatedBy($user2);
        $baseAddress->setDeletedAt($date);
        $baseAddress->setCreatedAt($date);
        $baseAddress->setUpdatedAt($date);

        $this->assertNull($baseAddress->getId());
        $this->assertEquals($city, $baseAddress->getCity());
        $this->assertEquals($country, $baseAddress->getCountry());
        $this->assertEquals($email, $baseAddress->getEmail());
        $this->assertEquals($street, $baseAddress->getStreet());
        $this->assertEquals($zipCode, $baseAddress->getZipCode());
        $this->assertEquals($user1, $baseAddress->getCreatedBy());
        $this->assertEquals($user2, $baseAddress->getUpdatedBy());
        $this->assertEquals($date, $baseAddress->getDeletedAt());
        $this->assertEquals($date, $baseAddress->getCreatedAt());
        $this->assertEquals($date, $baseAddress->getUpdatedAt());
    }
}
