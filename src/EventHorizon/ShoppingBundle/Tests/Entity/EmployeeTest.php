<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Employee;

class EmployeeTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $isBlocked = false;
        $isCompanyDirector = true;
        $user1 = "test user 1";
        $user2 = "test user 2";

        $company = new Company();
        $user = new User();

        $employee = new Employee();
        $employee->setCompany($company);
        $employee->setUser($user);
        $employee->setIsBlocked($isBlocked);
        $employee->setIsCompanyDirector($isCompanyDirector);
        $employee->setCreatedBy($user1);
        $employee->setUpdatedBy($user2);
        $employee->setDeletedAt($date);
        $employee->setCreatedAt($date);
        $employee->setUpdatedAt($date);

        $this->assertNull($employee->getId());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Company', $employee->getCompany());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Entity\User', $employee->getUser());
        $this->assertEquals($isBlocked, $employee->getIsBlocked());
        $this->assertEquals($isCompanyDirector, $employee->getIsCompanyDirector());
        $this->assertEquals($user1, $employee->getCreatedBy());
        $this->assertEquals($user2, $employee->getUpdatedBy());
        $this->assertEquals($date, $employee->getDeletedAt());
        $this->assertEquals($date, $employee->getCreatedAt());
        $this->assertEquals($date, $employee->getUpdatedAt());
    }
}
