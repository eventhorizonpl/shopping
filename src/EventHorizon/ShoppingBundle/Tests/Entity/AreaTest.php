<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\ShoppingBundle\Entity\Area;

class AreaTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $lft = 1;
        $lvl = 2;
        $name = "test name";
        $parent = new Area();
        $rgt = 3;
        $root = 4;
        $isVisible = true;
        $user1 = "test user 1";
        $user2 = "test user 2";

        $area = new Area();
        $area->setLft($lft);
        $area->setLvl($lvl);
        $area->setName($name);
        $area->setParent($parent);
        $area->setRgt($rgt);
        $area->setRoot($root);
        $area->setIsVisible($isVisible);
        $area->setCreatedBy($user1);
        $area->setUpdatedBy($user2);
        $area->setDeletedAt($date);
        $area->setCreatedAt($date);
        $area->setUpdatedAt($date);

        $this->assertNull($area->getId());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Area', $area->getParent());
        $area->setParentId($parent);
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Area', $area->getParentId());
        $area->setParent(null);
        $this->assertNull($area->getParent());
        $area->setParentId(null);
        $this->assertNull($area->getParentId());
        $this->assertEquals($lft, $area->getLft());
        $this->assertEquals($lvl, $area->getLvl());
        $this->assertEquals($name, $area->getName());
        $this->assertEquals($rgt, $area->getRgt());
        $this->assertEquals($root, $area->getRoot());
        $this->assertEquals($isVisible, $area->getIsVisible());
        $this->assertEquals($user1, $area->getCreatedBy());
        $this->assertEquals($user2, $area->getUpdatedBy());
        $this->assertEquals($date, $area->getDeletedAt());
        $this->assertEquals($date, $area->getCreatedAt());
        $this->assertEquals($date, $area->getUpdatedAt());
    }

    public function testToString()
    {
        $name = "test name";

        $area = new Area();
        $area->setName($name);

        $this->assertEquals($name, $area->__toString());
    }
}
