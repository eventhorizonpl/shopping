<?php

namespace EventHorizon\ShoppingBundle\Tests\Entity;

use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Contact;

class ContactTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $company = new Company();

        $contact = new Contact();
        $contact->setCompany($company);

        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Company', $contact->getCompany());
    }
}
