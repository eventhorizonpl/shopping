<?php

namespace EventHorizon\ShoppingBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use EventHorizon\ShoppingBundle\Entity\Company;

class PromotionRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

    public function testGetPromotionsByIdAndCompanyV1()
    {
        $promotionId = 100;
        $company = new Company();
        $this->em->persist($company);

        $promotion = $this->em
            ->getRepository('EventHorizonShoppingBundle:Promotion')
            ->getPromotionsByIdAndCompanyV1($promotionId, $company);

        $this->assertNull($promotion);
    }
}
