<?php

namespace EventHorizon\ShoppingBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;

class EmployeeRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

    public function testGetEmployeeByUserV1()
    {
        $user = new User();
        $this->em->persist($user);

        $employee = $this->em
            ->getRepository('EventHorizonShoppingBundle:Employee')
            ->getEmployeeByUserV1($user);

        $this->assertNull($employee);
    }

    public function testGetEmployeeByCompanyAndUserIdV1()
    {
        $company = new Company();
        $userId = 100;

        $this->em->persist($company);

        $employee = $this->em
            ->getRepository('EventHorizonShoppingBundle:Employee')
            ->getEmployeeByCompanyAndUserIdV1($company, $userId);

        $this->assertNull($employee);
    }
}
