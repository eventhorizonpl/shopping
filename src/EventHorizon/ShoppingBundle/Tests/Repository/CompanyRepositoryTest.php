<?php

namespace EventHorizon\ShoppingBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use EventHorizon\ShoppingBundle\Entity\Company;

class CompanyRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

    public function testGetCompanyByIdV1()
    {
        $id = 100;
        $company = $this->em
            ->getRepository('EventHorizonShoppingBundle:Company')
            ->getCompanyByIdV1($id);

        $this->assertNull($company);
    }
}
