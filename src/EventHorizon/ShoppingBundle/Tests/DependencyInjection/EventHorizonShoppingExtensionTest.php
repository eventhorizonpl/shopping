<?php

namespace EventHorizon\ShoppingBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\ShoppingBundle\DependencyInjection\EventHorizonShoppingExtension;

class EventHorizonShoppingExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonShoppingExtension();
        $config = array();
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\DependencyInjection\EventHorizonShoppingExtension', $loader);
    }
}
