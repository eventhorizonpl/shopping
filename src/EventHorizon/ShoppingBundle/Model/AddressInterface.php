<?php

namespace EventHorizon\ShoppingBundle\Model;

interface AddressInterface
{
    /**
     * Get city
     *
     * @return string
     */
    public function getCity();

    /**
     * Set city
     *
     * @param  string                                             $city
     * @return EventHorizon\ShoppingBundle\Model\AddressInterface
     */
    public function setCity($city);

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Set country
     *
     * @param  string                                             $country
     * @return EventHorizon\ShoppingBundle\Model\AddressInterface
     */
    public function setCountry($country);

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set email
     *
     * @param  string                                             $email
     * @return EventHorizon\ShoppingBundle\Model\AddressInterface
     */
    public function setEmail($email);

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet();

    /**
     * Set street
     *
     * @param  string                                             $street
     * @return EventHorizon\ShoppingBundle\Model\AddressInterface
     */
    public function setStreet($street);

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode();

    /**
     * Set zipCode
     *
     * @param  string                                             $zipCode
     * @return EventHorizon\ShoppingBundle\Model\AddressInterface
     */
    public function setZipCode($zipCode);
}
