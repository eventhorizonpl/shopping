<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trade
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="EventHorizon\ShoppingBundle\Repository\TradeRepository")
 * @ORM\Table(name="trade")
 */
class Trade
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\VisibleTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @ORM\OneToMany(mappedBy="parentId", targetEntity="EventHorizon\ShoppingBundle\Entity\Trade")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Trade")
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(inversedBy="children", targetEntity="EventHorizon\ShoppingBundle\Entity\Trade")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parentId;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="name", type="string")
     */
    private $name;

    /**
     * @var integer
     *
     * @Assert\Type(type="integer")
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @var integer
     *
     * @Assert\Type(type="integer")
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @var integer
     *
     * @Assert\Type(type="integer")
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @var integer
     *
     * @Assert\Type(type="integer")
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer")
     */
    private $root;

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getName()) ? : '';
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * @return Trade
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return Trade
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Trade
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Trade
     */
    public function getParent()
    {
        return $this->parentId;
    }

    /**
     * Get parentId
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Trade
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set parentId
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Trade $parentId
     * @return Trade
     */
    public function setParent(Trade $parentId = null)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Set parentId
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Trade $parentId
     * @return Trade
     */
    public function setParentId(Trade $parentId = null)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * @return Trade
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return Trade
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }
}
