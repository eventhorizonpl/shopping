<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\ShoppingBundle\Model\AddressInterface;

/**
 * BaseAddress
 *
 * @ORM\MappedSuperclass
 */
class BaseAddress implements AddressInterface
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="city", type="string")
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="country", type="string")
     */
    private $country;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="email", type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="street", type="string")
     */
    private $street;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="zip_code", type="string")
     */
    private $zipCode;

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param  string  $city
     * @return Contact
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param  string  $country
     * @return Contact
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param  string  $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street
     *
     * @param  string  $street
     * @return Contact
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set zipCode
     *
     * @param  string  $zipCode
     * @return Contact
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }
}
