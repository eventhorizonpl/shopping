<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;

/**
 * Promotion
 *
 * @ORM\Entity(repositoryClass="EventHorizon\ShoppingBundle\Repository\PromotionRepository")
 * @ORM\Table(name="promotion")
 */
class Promotion
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\VisibleTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @var integer $company
     *
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\ManyToOne(fetch="EXTRA_LAZY", inversedBy="promotions", targetEntity="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\JoinColumn(name="company_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $company;

    /**
     * @var integer $user
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\ManyToOne(fetch="EXTRA_LAZY", inversedBy="promotions", targetEntity="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @Assert\Type(type="float")
     * @ORM\Column(name="percent", type="float")
     */
    private $percent;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\Type(type="string")
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=255, name="slug", type="string")
     */
    private $slug;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="title", type="string")
     */
    private $title;

    /**
     * @var integer
     *
     * @Assert\Type(type="float")
     * @ORM\Column(name="upper_limit", type="integer")
     */
    private $upperLimit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="displaying_ends_at", type="datetime")
     */
    private $displayingEndsAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="displaying_starts_at", type="datetime")
     */
    private $displayingStartsAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promotion_ends_at", type="datetime")
     */
    private $promotionEndsAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promotion_starts_at", type="datetime")
     */
    private $promotionStartsAt;

    /**
     * @var boolean
     *
     * @Assert\Type(type="boolean")
     * @ORM\Column(name="is_accepted", type="boolean")
     */
    private $isAccepted;

    /**
     * Get company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Company $company
     * @return Promotion
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string    $description
     * @return Promotion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get displayingEndsAt
     *
     * @return \DateTime
     */
    public function getDisplayingEndsAt()
    {
        return $this->displayingEndsAt;
    }

    /**
     * Set displayingEndsAt
     *
     * @param  \DateTime $displayingEndsAt
     * @return Promotion
     */
    public function setDisplayingEndsAt($displayingEndsAt)
    {
        $this->displayingEndsAt = $displayingEndsAt;

        return $this;
    }

    /**
     * Get displayingStartsAt
     *
     * @return \DateTime
     */
    public function getDisplayingStartsAt()
    {
        return $this->displayingStartsAt;
    }

    /**
     * Set displayingStartsAt
     *
     * @param  \DateTime $displayingStartsAt
     * @return Promotion
     */
    public function setDisplayingStartsAt($displayingStartsAt)
    {
        $this->displayingStartsAt = $displayingStartsAt;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set isAccepted
     *
     * @param  boolean   $isAccepted
     * @return Promotion
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set percent
     *
     * @param  float     $percent
     * @return Promotion
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get promotionEndsAt
     *
     * @return \DateTime
     */
    public function getPromotionEndsAt()
    {
        return $this->promotionEndsAt;
    }

    /**
     * Set promotionEndsAt
     *
     * @param  \DateTime $promotionEndsAt
     * @return Promotion
     */
    public function setPromotionEndsAt($promotionEndsAt)
    {
        $this->promotionEndsAt = $promotionEndsAt;

        return $this;
    }

    /**
     * Get promotionStartsAt
     *
     * @return \DateTime
     */
    public function getPromotionStartsAt()
    {
        return $this->promotionStartsAt;
    }

    /**
     * Set promotionStartsAt
     *
     * @param  \DateTime $promotionStartsAt
     * @return Promotion
     */
    public function setPromotionStartsAt($promotionStartsAt)
    {
        $this->promotionStartsAt = $promotionStartsAt;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param  string    $slug
     * @return Promotion
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param  string    $title
     * @return Promotion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get upperLimit
     *
     * @return integer
     */
    public function getUpperLimit()
    {
        return $this->upperLimit;
    }

    /**
     * Set upperLimit
     *
     * @param  integer   $upperLimit
     * @return Promotion
     */
    public function setUpperLimit($upperLimit)
    {
        $this->upperLimit = $upperLimit;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EventHorizon\SecurityBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param  \EventHorizon\SecurityBundle\Entity\User $user
     * @return Promotion
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
