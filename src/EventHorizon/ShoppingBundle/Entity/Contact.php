<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\ShoppingBundle\Entity\Company;

/**
 * Contact
 *
 * @ORM\Entity(repositoryClass="EventHorizon\ShoppingBundle\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 */
class Contact extends BaseAddress
{
    /**
     * @var integer $company
     *
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\ManyToOne(fetch="EXTRA_LAZY", inversedBy="contacts", targetEntity="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\JoinColumn(name="company_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $company;

    /**
     * Get company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Company $company
     * @return Employee
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }
}
