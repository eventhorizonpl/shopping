<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Contact;
use EventHorizon\ShoppingBundle\Entity\Employee;
use EventHorizon\ShoppingBundle\Entity\InvoiceAddress;

/**
 * Company
 *
 * @ORM\Entity(repositoryClass="EventHorizon\ShoppingBundle\Repository\CompanyRepository")
 * @ORM\Table(name="company")
 */
class Company
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\BlockableTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @ORM\OneToMany(cascade={"persist"}, fetch="EXTRA_LAZY", mappedBy="company", targetEntity="EventHorizon\ShoppingBundle\Entity\Contact")
     */
    private $contacts;

    /**
     * @ORM\OneToMany(fetch="EXTRA_LAZY", mappedBy="company", targetEntity="EventHorizon\ShoppingBundle\Entity\Employee")
     */
    private $employees;

    /**
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\InvoiceAddress")
     * @ORM\OneToOne(cascade={"persist"}, fetch="EXTRA_LAZY", mappedBy="company", targetEntity="EventHorizon\ShoppingBundle\Entity\InvoiceAddress")
     */
    private $invoiceAddress;

    /**
     * @ORM\OneToMany(fetch="EXTRA_LAZY", mappedBy="company", targetEntity="EventHorizon\ShoppingBundle\Entity\Promotion")
     */
    private $promotions;

    /**
     * @var integer $user
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\OneToOne(fetch="EXTRA_LAZY", inversedBy="company", targetEntity="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @ORM\Column(name="bank_account", nullable=true, type="text")
     */
    private $bankAccount;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @ORM\Column(name="description", nullable=true, type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="nip", type="string")
     */
    private $nip;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="www", nullable=true, type="string")
     */
    private $www;

    /**
     * @var boolean
     *
     * @Assert\Type(type="boolean")
     * @ORM\Column(name="is_verified", type="boolean")
     */
    private $isVerified;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->promotions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getName()) ? : '';
    }

    /**
     * Get bankAccount
     *
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Set bankAccount
     *
     * @param  string  $bankAccount
     * @return Company
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * Add contact
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Contact $contact
     * @return Company
     */
    public function addContact(Contact $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Get contacts
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Remove contact
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Contact $contact
     */
    public function removeContact(Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string  $description
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add employee
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Employee $employee
     * @return Company
     */
    public function addEmployee(Employee $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Get employees
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Employee
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Remove employee
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Employee $employee
     */
    public function removeEmployee(Employee $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get invoiceAddress
     *
     * @return \EventHorizon\ShoppingBundle\Entity\InvoiceAddress
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * Set invoiceAddress
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\InvoiceAddress $invoiceAddress
     * @return Company
     */
    public function setInvoiceAddress(InvoiceAddress $invoiceAddress)
    {
        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set isVerified
     *
     * @param  boolean $isVerified
     * @return Company
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param  string  $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set nip
     *
     * @param  string  $nip
     * @return Company
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Add promotion
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Promotion $promotion
     * @return Company
     */
    public function addPromotion(Promotion $promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    /**
     * Get promotions
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Promotion
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Remove promotion
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Promotion $promotion
     */
    public function removePromotion(Promotion $promotion)
    {
        $this->promotions->removeElement($promotion);
    }

    /**
     * Get user
     *
     * @return \EventHorizon\SecurityBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param  \EventHorizon\SecurityBundle\Entity\User $user
     * @return Employee
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get www
     *
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Set www
     *
     * @param  string  $www
     * @return Company
     */
    public function setWww($www)
    {
        $this->www = $www;

        return $this;
    }
}
