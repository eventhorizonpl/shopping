<?php

namespace EventHorizon\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;

/**
 * Employee
 *
 * @ORM\Entity(repositoryClass="EventHorizon\ShoppingBundle\Repository\EmployeeRepository")
 * @ORM\Table(name="employee")
 */
class Employee
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\BlockableTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @var integer $company
     *
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\ManyToOne(fetch="EXTRA_LAZY", inversedBy="employees", targetEntity="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\JoinColumn(name="company_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $company;

    /**
     * @var integer $user
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\OneToOne(fetch="EXTRA_LAZY", inversedBy="employee", targetEntity="EventHorizon\SecurityBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    private $user;

    /**
     * @var boolean
     *
     * @Assert\Type(type="boolean")
     * @ORM\Column(name="is_company_director", type="boolean")
     */
    private $isCompanyDirector;

    /**
     * Get company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Company $company
     * @return Employee
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get isCompanyDirector
     *
     * @return boolean
     */
    public function getIsCompanyDirector()
    {
        return $this->isCompanyDirector;
    }

    /**
     * Set isCompanyDirector
     *
     * @param  boolean  $isCompanyDirector
     * @return Employee
     */
    public function setIsCompanyDirector($isCompanyDirector)
    {
        $this->isCompanyDirector = $isCompanyDirector;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EventHorizon\SecurityBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param  \EventHorizon\SecurityBundle\Entity\User $user
     * @return Employee
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
