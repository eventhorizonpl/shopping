<?php

namespace EventHorizon\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TypesDictionary
 *
 * @ORM\Entity(repositoryClass="EventHorizon\CoreBundle\Repository\TypesDictionaryRepository")
 * @ORM\Table(name="types_dictionary")
 */
class TypesDictionary
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Entity\VisibleTrait;
    use \EventHorizon\CoreBundle\Entity\SortableTrait;
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="description", type="string")
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="function", type="string")
     */
    private $function;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="object", type="string")
     */
    private $object;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="type", type="string")
     */
    private $type;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string          $description
     * @return TypesDictionary
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set function
     *
     * @param  string          $function
     * @return TypesDictionary
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param  string          $name
     * @return TypesDictionary
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get object
     *
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set object
     *
     * @param  string          $object
     * @return TypesDictionary
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param  string          $type
     * @return TypesDictionary
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
