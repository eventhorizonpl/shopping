<?php

namespace EventHorizon\CoreBundle\Entity;

trait BlameableTrait
{
    /**
     * @var string $createdBy
     *
     * @Assert\Type(type="string")
     * @Gedmo\Blameable(on="create")
     * @ORM\Column(name="created_by", nullable=true, type="string")
     */
    private $createdBy;

    /**
     * @var string $updatedBy
     *
     * @Assert\Type(type="string")
     * @Gedmo\Blameable(on="update")
     * @ORM\Column(name="updated_by", nullable=true, type="string")
     */
    private $updatedBy;

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
