<?php

namespace EventHorizon\CoreBundle\Entity;

trait BlockableTrait
{
    /**
     * @var boolean
     *
     * @Assert\Type(type="boolean")
     * @ORM\Column(name="is_blocked", type="boolean")
     */
    private $isBlocked;

    /**
     * Get isBlocked
     *
     * @return boolean
     */
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }

    /**
     * Set isBlocked
     *
     * @param boolean $isBlocked
     * @return $this
     */
    public function setIsBlocked($isBlocked)
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }
}
