<?php

namespace EventHorizon\CoreBundle\Entity;

trait SoftDeleteableTrait
{
    /**
     * @var datetime $deletedAt
     *
     * @ORM\Column(name="deleted_at", nullable=true, type="datetime")
     */
    private $deletedAt;

    /**
     * Get deleted_at
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set deleted_at
     *
     * @param \DateTime $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
