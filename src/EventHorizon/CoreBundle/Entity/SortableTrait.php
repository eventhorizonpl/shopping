<?php

namespace EventHorizon\CoreBundle\Entity;

trait SortableTrait
{
    /**
     * @var integer
     *
     * @Assert\Type(type="integer")
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param  integer         $position
     * @return TypesDictionary
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}
