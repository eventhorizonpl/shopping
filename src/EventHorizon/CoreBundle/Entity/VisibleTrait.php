<?php

namespace EventHorizon\CoreBundle\Entity;

trait VisibleTrait
{
    /**
     * @var boolean
     *
     * @Assert\Type(type="boolean")
     * @ORM\Column(name="is_visible", type="boolean")
     */
    private $isVisible;

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }
}
