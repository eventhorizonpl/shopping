<?php

namespace EventHorizon\CoreBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\CoreBundle\DependencyInjection\EventHorizonCoreExtension;

class EventHorizonCoreExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonCoreExtension();
        $config = array();
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\CoreBundle\DependencyInjection\EventHorizonCoreExtension', $loader);
    }
}
