<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

class Identifiable
{
    use \EventHorizon\CoreBundle\Entity\IdentifiableTrait;
}

class IdentifiableTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $identifiable = new Identifiable();

        $this->assertNull($identifiable->getId());
    }
}
