<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

class Timestampable
{
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;
}

class TimestampableTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();

        $timestampable = new Timestampable();
        $timestampable->setCreatedAt($date);
        $timestampable->setUpdatedAt($date);

        $this->assertEquals($date, $timestampable->getCreatedAt());
        $this->assertEquals($date, $timestampable->getUpdatedAt());
    }
}
