<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

class Sortable
{
    use \EventHorizon\CoreBundle\Entity\SortableTrait;
}

class SortableTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $position = 1;

        $sortable = new Sortable();
        $sortable->setPosition($position);

        $this->assertEquals($position, $sortable->getPosition());
    }
}
