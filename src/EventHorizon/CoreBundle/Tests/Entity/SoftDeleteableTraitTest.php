<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

class SoftDeleteable
{
    use \EventHorizon\CoreBundle\Entity\SoftDeleteableTrait;
}

class SoftDeleteableTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();

        $softDeleteable = new SoftDeleteable();
        $softDeleteable->setDeletedAt($date);

        $this->assertEquals($date, $softDeleteable->getDeletedAt());
    }
}
