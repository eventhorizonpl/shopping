<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

class Blameable
{
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
}

class BlameableTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $user1 = "test user 1";
        $user2 = "test user 2";

        $blameable = new Blameable();
        $blameable->setCreatedBy($user1);
        $blameable->setUpdatedBy($user2);

        $this->assertEquals($user1, $blameable->getCreatedBy());
        $this->assertEquals($user2, $blameable->getUpdatedBy());
    }
}
