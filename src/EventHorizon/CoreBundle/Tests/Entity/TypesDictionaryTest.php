<?php

namespace EventHorizon\CoreBundle\Tests\Entity;

use EventHorizon\CoreBundle\Entity\TypesDictionary;

class TypesDictionaryTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $date = new \DateTime();
        $description = "test description";
        $function = "test function";
        $name = "test name";
        $object = "test object";
        $position = 1;
        $type = "test type";
        $isVisible = true;
        $user1 = "test user 1";
        $user2 = "test user 2";

        $typesDictionary = new TypesDictionary();
        $typesDictionary->setDescription($description);
        $typesDictionary->setFunction($function);
        $typesDictionary->setName($name);
        $typesDictionary->setObject($object);
        $typesDictionary->setPosition($position);
        $typesDictionary->setType($type);
        $typesDictionary->setIsVisible($isVisible);
        $typesDictionary->setCreatedBy($user1);
        $typesDictionary->setUpdatedBy($user2);
        $typesDictionary->setDeletedAt($date);
        $typesDictionary->setCreatedAt($date);
        $typesDictionary->setUpdatedAt($date);

        $this->assertNull($typesDictionary->getId());
        $this->assertEquals($description, $typesDictionary->getDescription());
        $this->assertEquals($function, $typesDictionary->getFunction());
        $this->assertEquals($name, $typesDictionary->getName());
        $this->assertEquals($object, $typesDictionary->getObject());
        $this->assertEquals($position, $typesDictionary->getPosition());
        $this->assertEquals($type, $typesDictionary->getType());
        $this->assertEquals($isVisible, $typesDictionary->getIsVisible());
        $this->assertEquals($user1, $typesDictionary->getCreatedBy());
        $this->assertEquals($user2, $typesDictionary->getUpdatedBy());
        $this->assertEquals($date, $typesDictionary->getDeletedAt());
        $this->assertEquals($date, $typesDictionary->getCreatedAt());
        $this->assertEquals($date, $typesDictionary->getUpdatedAt());
    }
}
