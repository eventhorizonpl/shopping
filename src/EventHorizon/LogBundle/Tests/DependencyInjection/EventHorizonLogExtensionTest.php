<?php

namespace EventHorizon\LogBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\LogBundle\DependencyInjection\EventHorizonLogExtension;

class EventHorizonLogExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonLogExtension();
        $config = array();
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\LogBundle\DependencyInjection\EventHorizonLogExtension', $loader);
    }
}
