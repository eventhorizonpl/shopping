<?php

namespace EventHorizon\LogBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

use EventHorizon\LogBundle\Service\Log;

class LogTest extends WebTestCase
{
    /**
     * @var \EventHorizon\LogBundle\Service\Log
     */
    private $log;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $dm = static::$kernel->getContainer()->get('doctrine_mongodb');
        $request = new Request();
        $this->log = new Log($dm, $request);
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testAddLog()
    {
        $content = "test content";
        $level = "test level";
        $log = $this->log->add($content, $level);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
        $this->assertEquals($level, $log->getLevel());
    }

    public function testAddDebug()
    {
        $content = "test content";
        $log = $this->log->addDebug($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddInfo()
    {
        $content = "test content";
        $log = $this->log->addInfo($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddNotice()
    {
        $content = "test content";
        $log = $this->log->addNotice($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddWarning()
    {
        $content = "test content";
        $log = $this->log->addWarning($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddError()
    {
        $content = "test content";
        $log = $this->log->addError($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddCritical()
    {
        $content = "test content";
        $log = $this->log->addCritical($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddAlert()
    {
        $content = "test content";
        $log = $this->log->addAlert($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testAddEmergency()
    {
        $content = "test content";
        $log = $this->log->addEmergency($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertEquals($content, $log->getContent());
    }

    public function testLogAlert()
    {
        $content = "test content";
        $log = $this->log->logAlert($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogCritical()
    {
        $content = "test content";
        $log = $this->log->logCritical($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogDebug()
    {
        $content = "test content";
        $log = $this->log->logDebug($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogEmergency()
    {
        $content = "test content";
        $log = $this->log->logEmergency($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogError()
    {
        $content = "test content";
        $log = $this->log->logError($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogInfo()
    {
        $content = "test content";
        $log = $this->log->logInfo($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogNotice()
    {
        $content = "test content";
        $log = $this->log->logNotice($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }

    public function testLogWarning()
    {
        $content = "test content";
        $log = $this->log->logWarning($content);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
        $this->assertStringEndsWith($content, $log->getContent());
    }
}
