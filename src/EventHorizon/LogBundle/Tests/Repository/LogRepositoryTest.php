<?php

namespace EventHorizon\LogBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LogRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $dm;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->dm = static::$kernel->getContainer()->get('doctrine_mongodb')->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->dm->close();
    }

    public function testAddLog()
    {
        $content = "test content";
        $level = "test level";
        $log = $this->dm
            ->getRepository('EventHorizonLogBundle:Log')
            ->addLog($content, $level);

        $this->assertInstanceOf('\EventHorizon\LogBundle\Document\Log', $log);
    }
}
