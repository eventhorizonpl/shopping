<?php

namespace EventHorizon\LogBundle\Tests\Document;

use EventHorizon\LogBundle\Document\Log;

class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $content = "test content";
        $level = "level";
        $date = new \DateTime();

        $log = new Log();
        $log->setContent($content);
        $log->setLevel($level);
        $log->setCreatedAt($date);

        $this->assertNull($log->getId());
        $this->assertEquals($content, $log->getContent());
        $this->assertEquals($level, $log->getLevel());
        $this->assertEquals($date, $log->getCreatedAt());
    }

    public function testPrePersist()
    {
        $log = new Log();
        $log->prePersist();

        $this->assertInstanceOf('\MongoTimestamp', $log->getCreatedAt());
    }
}
