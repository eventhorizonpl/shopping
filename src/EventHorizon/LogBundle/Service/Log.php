<?php

namespace EventHorizon\LogBundle\Service;

class Log
{
    private $dm;
    private $ip;

    private $level_debug = 100;
    private $level_info = 200;
    private $level_notice = 250;
    private $level_warning = 300;
    private $level_error = 400;
    private $level_critical = 500;
    private $level_alert = 550;
    private $level_emergency = 600;

    public function __construct(\Doctrine\Bundle\MongoDBBundle\ManagerRegistry $doctrine_mongodb_service, \Symfony\Component\HttpFoundation\Request $request)
    {
        $this->dm = $doctrine_mongodb_service->getManager();
        $this->ip = $request->getClientIp();
    }

    public function add($content, $level)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $level);
    }

    public function addAlert($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_alert);
    }

    public function addCritical($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_critical);
    }

    public function addDebug($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_debug);
    }

    public function addEmergency($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_emergency);
    }

    public function addError($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_error);
    }

    public function addInfo($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_info);
    }

    public function addNotice($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_notice);
    }

    public function addWarning($content)
    {
        return $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_warning);
    }

    public function logAlert($message)
    {
        return $this->addAlert($this->ip." ".$message);
    }

    public function logCritical($message)
    {
        return $this->addCritical($this->ip." ".$message);
    }

    public function logDebug($message)
    {
        return $this->addDebug($this->ip." ".$message);
    }

    public function logEmergency($message)
    {
        return $this->addEmergency($this->ip." ".$message);
    }

    public function logError($message)
    {
        return $this->addError($this->ip." ".$message);
    }

    public function logInfo($message)
    {
        return $this->addInfo($this->ip." ".$message);
    }

    public function logNotice($message)
    {
        return $this->addNotice($this->ip." ".$message);
    }

    public function logWarning($message)
    {
        return $this->addWarning($this->ip." ".$message);
    }
}
