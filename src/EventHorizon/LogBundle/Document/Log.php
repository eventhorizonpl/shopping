<?php

namespace EventHorizon\LogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="EventHorizon\LogBundle\Repository\LogRepository")
 * @HasLifecycleCallbacks
 */
class Log
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @MongoDB\String
     */
    protected $content;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @MongoDB\String
     */
    protected $level;

    /**
     * @MongoDB\Timestamp
     */
    private $created_at;

    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param  string $content
     * @return Log
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get level
     *
     * @return string $level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set level
     *
     * @param  string $level
     * @return Log
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return timestamp $createdAt
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param  string $created_at
     * @return Log
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @MongoDB\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \MongoTimestamp());
    }
}
