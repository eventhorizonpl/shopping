EventHorizonLogBundle
=====================

This bundle is designed to store log messages in the MongodDB.

## Installation

Add EventHorizonLogBundle in your composer.json:

```js
{
    "require": {
        // ...
        "eventhorizon/log-bundle": "dev-master"
    }
}
```

Now tell composer to download the bundle by running the command:

``` bash
$ php composer.phar update eventhorizon/log-bundle
```

## Configuration

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new EventHorizon\LogBundle\EventHorizonLogBundle(),
    );
}
```


## Usage - example

``` php
<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Journal;

class JournalController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
    }
// ...
    public function showAction($id)
    {
        $this->common();
        $this->entity = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdV1($id, $this->user_id);

        if (!($this->entity)) {
            $this->message = "EventHorizonRpgBundle:Journal:show id=".$id." user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($this->message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        return $this->render('EventHorizonRpgBundle:Journal:show.html.twig', array(
            'entity' => $this->entity,
            'setup'  => $this->setup,
        ));
    }
}

<?php

namespace EventHorizon\RpgBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EventHorizonRpgBundle extends Bundle
{
    public static function addNotice($message, $log, $request)
    {
        $request->trustProxyData();
        $ip = $request->getClientIp();
        $message = $ip." ".$message;
        $log->addNotice($message);
    }
// ...
}
```

## Usage - methods and standard log levels

add($content, $level)
addDebug($content)
addInfo($content)
addNotice($content)
addWarning($content)
addError($content)
addCritical($content)
addAlert($content)
addEmergency($content)

level_debug = 100
level_info = 200
level_notice = 250
level_warning = 300
level_error = 400
level_critical = 500
level_alert = 550
level_emergency = 600
