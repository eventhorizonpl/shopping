<?php

namespace EventHorizon\SecurityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EventHorizonSecurityBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
