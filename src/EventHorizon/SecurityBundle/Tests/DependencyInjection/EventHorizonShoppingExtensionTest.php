<?php

namespace EventHorizon\SecurityBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\SecurityBundle\DependencyInjection\EventHorizonSecurityExtension;

class EventHorizonSecurityExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonSecurityExtension();
        $config = array();
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\SecurityBundle\DependencyInjection\EventHorizonSecurityExtension', $loader);
    }
}
