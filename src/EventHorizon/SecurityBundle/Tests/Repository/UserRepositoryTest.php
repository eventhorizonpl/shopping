<?php

namespace EventHorizon\SecurityBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use EventHorizon\SecurityBundle\Entity\User;

class UserRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     */
    private $userManager;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

    /**
     * @expectedException Doctrine\DBAL\DBALException
     */
    public function testSaveNewUserException()
    {
        $company = null;
        $type = "user";
        $user = new User();
        $newUser = $this->em
            ->getRepository('EventHorizonSecurityBundle:User')
            ->saveNewUser($company, $type, $user, $this->userManager);
    }
}
