<?php

namespace EventHorizon\SecurityBundle\Tests\Entity;

use EventHorizon\SecurityBundle\Entity\User;
use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Employee;
use EventHorizon\ShoppingBundle\Entity\Promotion;

class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $company = new Company();
        $date = new \DateTime();
        $employee = new Employee();
        $firstName = "test first name";
        $lastName = "test last name";
        $user1 = "test user 1";
        $user2 = "test user 2";

        $promotion = new Promotion();

        $user = new User();
        $user->addPromotion($promotion);
        $user->setCompany($company);
        $user->setEmployee($employee);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCreatedBy($user1);
        $user->setUpdatedBy($user2);
        $user->setCreatedAt($date);
        $user->setUpdatedAt($date);

        $this->assertNull($user->getId());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Company', $user->getCompany());
        $this->assertInstanceOf('\EventHorizon\ShoppingBundle\Entity\Employee', $user->getEmployee());
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $user->getPromotions());
        $this->assertCount(1, $user->getPromotions());
        $user->removePromotion($promotion);
        $this->assertCount(0, $user->getPromotions());
        $this->assertEquals($firstName, $user->getFirstName());
        $this->assertEquals($lastName, $user->getLastName());
        $this->assertEquals($user1, $user->getCreatedBy());
        $this->assertEquals($user2, $user->getUpdatedBy());
        $this->assertEquals($date, $user->getCreatedAt());
        $this->assertEquals($date, $user->getUpdatedAt());
    }
}
