<?php

namespace EventHorizon\SecurityBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewRegistrationControllerTest extends WebTestCase
{
    public function testRegisterCompany()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register/company');

        $this->assertTrue($crawler->filter('html:contains("Rejestracja")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa firmy")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("NIP")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);

        $form = $crawler->selectButton('Zarejestruj')->form();

        $form['fos_user_registration_form[username]'] = 'a';
        $form['fos_user_registration_form[email]'] = 'a@a.com';
        $form['fos_user_registration_form[plainPassword][first]'] = 'a';
        $form['fos_user_registration_form[plainPassword][second]'] = 'a';
        $form['fos_user_registration_form[firstName]'] = 'a';
        $form['fos_user_registration_form[lastName]'] = 'a';
        $form['eventhorizon_securitybundle_RegistrationCompanyType[name]'] = 'a';
        $form['eventhorizon_securitybundle_RegistrationCompanyType[nip]'] = 'a';

        $crawler = $client->submit($form);

        $this->assertTrue($crawler->filter('html:contains("The username is too short")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("The password is too short")')->count() > 0);

        $form['fos_user_registration_form[username]'] = 'companyusertest1';
        $form['fos_user_registration_form[email]'] = 'companyusertest1@companyusertest1.com';
        $form['fos_user_registration_form[plainPassword][first]'] = 'companyusertest1';
        $form['fos_user_registration_form[plainPassword][second]'] = 'companyusertest1';
        $form['fos_user_registration_form[firstName]'] = 'companyusertest1';
        $form['fos_user_registration_form[lastName]'] = 'companyusertest1';
        $form['eventhorizon_securitybundle_RegistrationCompanyType[name]'] = 'companyusertest1';
        $form['eventhorizon_securitybundle_RegistrationCompanyType[nip]'] = 'companyusertest1';

        $crawler = $client->submit($form);

        $this->assertTrue($crawler->filter('html:contains("The user has been created successfully")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Congrats companyusertest1, your account is now activated.")')->count() > 0);
    }

    public function testRegisterUser()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register/user');

        $this->assertTrue($crawler->filter('html:contains("Rejestracja")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);

        $form = $crawler->selectButton('Zarejestruj')->form();

        $form['fos_user_registration_form[username]'] = 'a';
        $form['fos_user_registration_form[email]'] = 'a@a.com';
        $form['fos_user_registration_form[plainPassword][first]'] = 'a';
        $form['fos_user_registration_form[plainPassword][second]'] = 'a';
        $form['fos_user_registration_form[firstName]'] = 'a';
        $form['fos_user_registration_form[lastName]'] = 'a';

        $crawler = $client->submit($form);

        $this->assertTrue($crawler->filter('html:contains("The username is too short")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("The password is too short")')->count() > 0);

        $form['fos_user_registration_form[username]'] = 'customertest1';
        $form['fos_user_registration_form[email]'] = 'customertest1@customertest1.com';
        $form['fos_user_registration_form[plainPassword][first]'] = 'customertest1';
        $form['fos_user_registration_form[plainPassword][second]'] = 'customertest1';
        $form['fos_user_registration_form[firstName]'] = 'customertest1';
        $form['fos_user_registration_form[lastName]'] = 'customertest1';

        $crawler = $client->submit($form);

        $this->assertTrue($crawler->filter('html:contains("The user has been created successfully")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Congrats customertest1, your account is now activated.")')->count() > 0);
    }

    public function testRegisterWrongArgument()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register/wrong_argument');

        $this->assertTrue($crawler->filter('html:contains("404 Not Found")')->count() > 0);
    }
}
