<?php

namespace EventHorizon\SecurityBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public function testRegister()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/register');

        $this->assertTrue($crawler->filter('html:contains("Rejestracja")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);

        $crawler = $client->request('GET', '/register/');

        $this->assertTrue($crawler->filter('html:contains("Rejestracja")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Imię")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwisko")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Powtórz hasło")')->count() > 0);
    }
}
