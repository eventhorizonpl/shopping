<?php

namespace EventHorizon\SecurityBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        $url = $this->container->get('router')->generate('register', array('type' => 'user'));

        return new RedirectResponse($url);
    }
}
