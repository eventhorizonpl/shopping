<?php

namespace EventHorizon\SecurityBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use EventHorizon\SecurityBundle\Form\RegistrationCompanyType;
use EventHorizon\ShoppingBundle\Entity\Company;

class NewRegistrationController extends Controller
{
    /**
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $em;

    /**
     * @DI\Inject("request")
     */
    private $request;

    /**
     * @Method({"GET", "POST"})
     * @Route("/register/{type}", name="register")
     * @Template("EventHorizonSecurityBundle:Registration:register.html.twig")
     */
    public function registerAction($type)
    {
        if (!(in_array($type, array("company", "user")))) {
            throw $this->createNotFoundException('Wrong type parameter.');
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, new UserEvent($user, $this->request));

        $form = $formFactory->createForm();
        $form->setData($user);

        if ("company" === $type) {
            $company = new Company();
            $companyForm = $this->createForm(new RegistrationCompanyType(), $company);
        } else {
            $company = null;
            $companyForm = null;
        }

        if ('POST' === $this->request->getMethod()) {
            $form->handleRequest($this->request);

            if ("company" === $type) {
                $companyForm->handleRequest($this->request);
            }

            if (($form->isValid()) and (("user" === $type) or (("company" === $type) and ($companyForm->isValid())))) {
                $event = new FormEvent($form, $this->request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $this->em->getRepository('EventHorizonSecurityBundle:User')->saveNewUser($company, $type, $user, $userManager);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $this->request, $response));

                return $response;
            }
        }

        return array(
            'companyForm' => ($companyForm) ? $companyForm->createView() : $companyForm,
            'form'        => $form->createView(),
            'type'        => $type,
        );
    }
}
