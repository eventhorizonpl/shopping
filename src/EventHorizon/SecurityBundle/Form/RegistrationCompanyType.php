<?php

namespace EventHorizon\SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'constraints' => array(
                    new Length(array("max" => 255)),
                    new NotBlank(),
                )
            ))
            ->add('nip', 'text', array(
                'constraints' => array(
                    new Length(array("max" => 255)),
                    new NotBlank(),
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\ShoppingBundle\Entity\Company'
        ));
    }

    public function getName()
    {
        return 'eventhorizon_securitybundle_RegistrationCompanyType';
    }
}
