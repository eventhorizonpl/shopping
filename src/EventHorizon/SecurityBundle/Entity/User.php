<?php

namespace EventHorizon\SecurityBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use EventHorizon\ShoppingBundle\Entity\Company;
use EventHorizon\ShoppingBundle\Entity\Employee;
use EventHorizon\ShoppingBundle\Entity\Promotion;

/**
 * @ORM\Entity(repositoryClass="EventHorizon\SecurityBundle\Repository\UserRepository")
 * @ORM\Table(name="security_user")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser
{
    use \EventHorizon\CoreBundle\Entity\BlameableTrait;
    use \EventHorizon\CoreBundle\Entity\TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Company")
     * @ORM\OneToOne(fetch="EXTRA_LAZY", mappedBy="user", targetEntity="EventHorizon\ShoppingBundle\Entity\Company")
     */
    private $company;

    /**
     * @Assert\Type(type="EventHorizon\ShoppingBundle\Entity\Employee")
     * @ORM\OneToOne(fetch="EXTRA_LAZY", mappedBy="user", targetEntity="EventHorizon\ShoppingBundle\Entity\Employee")
     */
    private $employee;

    /**
     * @ORM\OneToMany(fetch="EXTRA_LAZY", mappedBy="user", targetEntity="EventHorizon\ShoppingBundle\Entity\Promotion")
     */
    private $promotions;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="first_name", type="string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @Assert\Length(
     *   max = "255"
     * )
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @ORM\Column(length=255, name="last_name", type="string")
     */
    private $lastName;

    public function __construct()
    {
        parent::__construct();

        $this->promotions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get company
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Company $company
     * @return User
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set employee
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Employee $employee
     * @return User
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param  string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param  string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Add promotion
     *
     * @param  \EventHorizon\ShoppingBundle\Entity\Promotion $promotion
     * @return User
     */
    public function addPromotion(Promotion $promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    /**
     * Get promotions
     *
     * @return \EventHorizon\ShoppingBundle\Entity\Promotion
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Remove promotion
     *
     * @param \EventHorizon\ShoppingBundle\Entity\Promotion $promotion
     */
    public function removePromotion(Promotion $promotion)
    {
        $this->promotions->removeElement($promotion);
    }
}
