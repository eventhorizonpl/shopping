<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class ContactAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/contact/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Company")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("City")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Email")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Street")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Zip Code")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/contact/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o kontakcie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("City")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Country")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Email")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Street")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Zip Code")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
