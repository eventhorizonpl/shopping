<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class LogAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/log/log/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Content")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Level")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Created At")')->count() > 0);
    }
}
