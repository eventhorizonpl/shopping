<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseAdminTest extends WebTestCase
{
    protected $client;

    public function setUp()
    {
        $this->client = static::createClient();

        $this->client->followRedirects(true);
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function loginAsAdmin()
    {
        $crawler = $this->client->request('GET', '/login');

        $this->assertTrue($crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'admin';
        $form['_password'] = 'admin';

        $crawler = $this->client->submit($form);
    }
}
