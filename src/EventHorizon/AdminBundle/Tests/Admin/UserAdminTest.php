<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class UserAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/security/user/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Username")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Email")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Roles")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Enabled")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Locked")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Expired")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/security/user/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Email")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Username")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Roles")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Enabled")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Locked")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Expired")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
