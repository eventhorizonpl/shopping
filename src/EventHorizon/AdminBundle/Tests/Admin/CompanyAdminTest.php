<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class CompanyAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/company/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("User")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nip")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Verified")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Blocked")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/company/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o firmie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Bank Account")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Nip")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Www")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Verified")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Blocked")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
