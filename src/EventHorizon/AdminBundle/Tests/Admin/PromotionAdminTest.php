<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class PromotionAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/promotion/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Company")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("User")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Percent")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Title")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Upper Limit")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Displaying Starts At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Displaying Ends At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promotion Starts At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promotion Ends At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Accepted")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/promotion/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o promocji")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Percent")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Title")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Upper Limit")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Displaying Starts At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Displaying Ends At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promotion Starts At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Promotion Ends At")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Accepted")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
