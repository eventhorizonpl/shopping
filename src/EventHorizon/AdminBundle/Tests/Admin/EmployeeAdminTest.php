<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class EmployeeAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/employee/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Company")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("User")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Company Director")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/employee/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o pracowniku")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Company Director")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
