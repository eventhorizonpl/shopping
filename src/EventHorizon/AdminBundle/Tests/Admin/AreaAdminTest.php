<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class AreaAdminTest extends BaseAdminTest
{
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/area/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);
    }

    public function testCreate()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/area/create');
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Parent Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);

        $form = $crawler->selectButton('Create')->form();

        $crawler = $this->client->submit($form);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/shopping/area/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Parent Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
}
