<?php

namespace EventHorizon\AdminBundle\Tests\Admin;

class TypesDictionaryAdminTest extends BaseAdminTest
{
/*
    public function testList()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/core/typesdictionary/list');
        $this->assertTrue($crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);
    }
*/
    public function testCreate()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/core/typesdictionary/create');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o typie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dodatkowe informacje o typie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Function")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Object")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Type")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Position")')->count() > 0);

        $form = $crawler->selectButton('Create')->form();

        $crawler = $this->client->submit($form);
    }
/*
    public function testEdit()
    {
        $this->loginAsAdmin();

        $crawler = $this->client->request('GET', '/admin/eventhorizon/core/typesdictionary/1/edit');
        $this->assertTrue($crawler->filter('html:contains("Ogólne informacje o typie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Dodatkowe informacje o typie")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Function")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Object")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Type")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Is Visible")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Position")')->count() > 0);

        $form = $crawler->selectButton('Update')->form();

        $crawler = $this->client->submit($form);
    }
*/
}
