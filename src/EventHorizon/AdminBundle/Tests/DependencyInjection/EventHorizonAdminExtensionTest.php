<?php

namespace EventHorizon\AdminBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\AdminBundle\DependencyInjection\EventHorizonAdminExtension;

class EventHorizonAdminExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonAdminExtension();
        $config = array();
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\AdminBundle\DependencyInjection\EventHorizonAdminExtension', $loader);
    }
}
