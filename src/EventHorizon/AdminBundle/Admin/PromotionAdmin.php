<?php

namespace EventHorizon\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PromotionAdmin extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('company')
            ->add('user')
            ->add('description')
            ->add('percent')
            ->add('title')
            ->add('upperLimit')
            ->add('displayingStartsAt')
            ->add('displayingEndsAt')
            ->add('promotionStartsAt')
            ->add('promotionEndsAt')
            ->add('isAccepted')
            ->add('isVisible')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o promocji'))
                ->add('description', null, array('help' => 'Opis'))
                ->add('percent', null, array('help' => 'Procent'))
                ->add('title', null, array('help' => 'Tytuł'))
                ->add('upperLimit', null, array('help' => 'Górny limit'))
                ->add('isAccepted', null, array('help' => 'Akceptacja ze strony portalu'))
                ->add('isVisible', null, array('help' => 'Jest widoczna'))
            ->end()
            ->with('Dodatkowe informacje', array('description' => 'Dodatkowe informacje o promocji'))
                ->add('displayingStartsAt', null, array('help' => 'Wyświetlanie rozpoczyna się'))
                ->add('displayingEndsAt', null, array('help' => 'Wyświetlanie kończy się'))
                ->add('promotionStartsAt', null, array('help' => 'Promocja rozpoczyna się'))
                ->add('promotionEndsAt', null, array('help' => 'Promocja kończy się'))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('company')
            ->add('user')
            ->add('description')
            ->add('percent')
            ->add('title')
            ->add('upperLimit')
            ->add('displayingStartsAt')
            ->add('displayingEndsAt')
            ->add('promotionStartsAt')
            ->add('promotionEndsAt')
            ->add('isAccepted')
            ->add('isVisible')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
