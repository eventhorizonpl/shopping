<?php

namespace EventHorizon\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ContactAdmin extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('company')
            ->add('city')
            ->add('country')
            ->add('email')
            ->add('street')
            ->add('zipCode')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o kontakcie'))
                ->add('city', null, array('help' => 'Miasto'))
                ->add('country', null, array('help' => 'Kraj'))
                ->add('email', null, array('help' => 'E-mail'))
                ->add('street', null, array('help' => 'Ulica'))
                ->add('zipCode', null, array('help' => 'Kod pocztowy'))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('company')
            ->add('city')
            ->add('email')
            ->add('street')
            ->add('zipCode')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
