<?php

namespace EventHorizon\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('locked')
            ->add('expired')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o użytkowniku'))
                ->add('email', null, array('help' => 'E-mail'))
                ->add('username', null, array('help' => 'Nazwa użytkownika'))
            ->end()
            ->with('Ustawienia konta', array('description' => 'Dodatkowe ustawienia konta'))
                ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
                        'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
                        'ROLE_USER' => 'ROLE_USER',
                        'ROLE_MERCHANT' => 'ROLE_MERCHANT',
                        // customer
                        'ROLE_CUSTOMER_USER' => 'ROLE_CUSTOMER_USER',
                        // company
                        'ROLE_COMPANY_ACCOUNT_MANAGER' => 'ROLE_COMPANY_ACCOUNT_MANAGER',
                        'ROLE_COMPANY_DIRECTOR' => 'ROLE_COMPANY_DIRECTOR',
                        'ROLE_COMPANY_USER' => 'ROLE_COMPANY_USER',
                        'ROLE_HUMAN_RESOURCE_MANAGER' => 'ROLE_HUMAN_RESOURCE_MANAGER',
                        'ROLE_PROMOTION_MANAGER' => 'ROLE_PROMOTION_MANAGER',
                    ),
                    'help'     => 'Uprawnienia',
                    'multiple' => true,
                    'required' => false,
                ))
                ->add('enabled', null, array(
                    'help'     => 'Włączone',
                    'required' => false,
                ))
                ->add('locked', null, array(
                    'help'     => 'Zablokowane',
                    'required' => false,
                ))
                ->add('expired', null, array(
                    'help'     => 'Zdezaktualizowane',
                    'required' => false,
                ))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('username')
            ->add('email')
            ->add('roles')
            ->add('enabled')
            ->add('locked')
            ->add('expired')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
