<?php

namespace EventHorizon\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CompanyAdmin extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('user')
            ->add('name')
            ->add('nip')
            ->add('isVerified')
            ->add('isBlocked')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o firmie'))
                ->add('name', null, array('help' => 'Nazwa firmy'))
                ->add('description', null, array('help' => 'Opis'))
                ->add('bankAccount', null, array('help' => 'Konto bankowe'))
                ->add('nip', null, array('help' => 'NIP firmy'))
                ->add('www', null, array('help' => 'Strona www'))
            ->end()
            ->with('Dodatkowe informacje', array('description' => 'Dodatkowe informacje o firmie'))
                ->add('isVerified', null, array(
                    'help'     => 'Zweryfikowana',
                    'required' => false,
                ))
                ->add('isBlocked', null, array(
                    'help'     => 'Zablokowana',
                    'required' => false,
                ))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('name')
            ->add('nip')
            ->add('isVerified')
            ->add('isBlocked')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
