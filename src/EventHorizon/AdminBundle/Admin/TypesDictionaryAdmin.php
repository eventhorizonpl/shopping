<?php

namespace EventHorizon\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TypesDictionaryAdmin extends Admin
{
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('function')
            ->add('name')
            ->add('object')
            ->add('type')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o typie'))
                ->add('description', null, array('help' => 'Opis'))
                ->add('function', null, array('help' => 'Funkcja'))
                ->add('name', null, array('help' => 'Nazwa'))
                ->add('object', null, array('help' => 'Obiekt'))
                ->add('type', null, array('help' => 'Typ'))
            ->end()
            ->with('Dodatkowe informacje', array('description' => 'Dodatkowe informacje o typie'))
                ->add('isVisible', null, array(
                    'help'     => 'Widoczność',
                    'required' => false,
                ))
                ->add('position', null, array(
                    'help'     => 'Pozycja',
                    'required' => false,
                ))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('function')
            ->add('name')
            ->add('object')
            ->add('type')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }
}
