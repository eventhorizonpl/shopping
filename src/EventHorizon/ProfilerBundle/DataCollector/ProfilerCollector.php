<?php

namespace EventHorizon\ProfilerBundle\DataCollector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class ProfilerCollector extends DataCollector
{
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $defined_functions = get_defined_functions();
        $this->data = array(
            'declared_classes' => get_declared_classes(),
            'declared_classes_count' => count(get_declared_classes()),
            'declared_interfaces' => get_declared_interfaces(),
            'declared_interfaces_count' => count(get_declared_interfaces()),
            'defined_functions_internal' => $defined_functions["internal"],
            'defined_functions_internal_count' => count($defined_functions["internal"]),
            'defined_functions_user' => $defined_functions["user"],
            'defined_functions_user_count' => count($defined_functions["user"]),
        );
    }

    public function getDeclaredClasses()
    {
        return $this->data['declared_classes'];
    }

    public function getDeclaredClassesCount()
    {
        return $this->data['declared_classes_count'];
    }

    public function getDeclaredInterfaces()
    {
        return $this->data['declared_interfaces'];
    }

    public function getDeclaredInterfacesCount()
    {
        return $this->data['declared_interfaces_count'];
    }

    public function getDefinedFunctionsInternal()
    {
        return $this->data['defined_functions_internal'];
    }

    public function getDefinedFunctionsInternalCount()
    {
        return $this->data['defined_functions_internal_count'];
    }

    public function getDefinedFunctionsUser()
    {
        return $this->data['defined_functions_user'];
    }

    public function getDefinedFunctionsUserCount()
    {
        return $this->data['defined_functions_user_count'];
    }

    public function getName()
    {
        return 'profiler';
    }
}
