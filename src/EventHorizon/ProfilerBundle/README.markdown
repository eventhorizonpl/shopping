EventHorizonProfilerBundle
==========================

This bundle is designed to improve profiling.

## Installation

Add EventHorizonProfilerBundle in your composer.json:

```js
{
    "require": {
        // ...
        "eventhorizon/profiler-bundle": "dev-master"
    }
}
```

Now tell composer to download the bundle by running the command:

``` bash
$ php composer.phar update eventhorizon/profiler-bundle
```

## Configuration

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    if (in_array($this->getEnvironment(), array('dev', 'test'))) {
        // ...
        $bundles[] = new EventHorizon\ProfilerBundle\EventHorizonProfilerBundle();
    }
}
```

``` yaml
# app/config/config_dev.yml

services:
    data_collector.profiler:
        class: EventHorizon\ProfilerBundle\DataCollector\ProfilerCollector
        tags:
            - { name: data_collector, template: "EventHorizonProfilerBundle:Collector:profiler", id: "profiler" }
```

