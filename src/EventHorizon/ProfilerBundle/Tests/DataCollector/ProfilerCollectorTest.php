<?php

namespace EventHorizon\ProfilerBundle\Tests\DataCollector;

use EventHorizon\ProfilerBundle\DataCollector\ProfilerCollector;

class ProfilerCollectorTest extends \PHPUnit_Framework_TestCase
{
    protected $profilerCollector;

    protected function setUp()
    {
        $this->profilerCollector = new ProfilerCollector();
        $request = $this->getMock('\Symfony\Component\HttpFoundation\Request');
        $response = $this->getMock('\Symfony\Component\HttpFoundation\Response');
        $this->profilerCollector->collect($request, $response);
    }

    public function testGetDeclaredClasses()
    {
        $declaredClasses = $this->profilerCollector->getDeclaredClasses();
        $this->assertGreaterThan(1, count($declaredClasses));
    }

    public function testGetDeclaredClassesCount()
    {
        $declaredClassesCount = $this->profilerCollector->getDeclaredClassesCount();
        $this->assertGreaterThan(1, $declaredClassesCount);
    }

    public function testGetDeclaredInterfaces()
    {
        $declaredInterfaces = $this->profilerCollector->getDeclaredInterfaces();
        $this->assertGreaterThan(1, count($declaredInterfaces));
    }

    public function testGetDeclaredInterfacesCount()
    {
        $declaredInterfacesCount = $this->profilerCollector->getDeclaredInterfacesCount();
        $this->assertGreaterThan(1, $declaredInterfacesCount);
    }

    public function testGetDefinedFunctionsInternal()
    {
        $definedFunctionsInternal = $this->profilerCollector->getDefinedFunctionsInternal();
        $this->assertGreaterThan(1, count($definedFunctionsInternal));
    }

    public function testGetDefinedFunctionsInternalCount()
    {
        $definedFunctionsInternalCount = $this->profilerCollector->getDefinedFunctionsInternalCount();
        $this->assertGreaterThan(1, $definedFunctionsInternalCount);
    }

    public function testGetDefinedFunctionsUser()
    {
        $definedFunctionsUser = $this->profilerCollector->getDefinedFunctionsUser();
        $this->assertGreaterThan(1, count($definedFunctionsUser));
    }

    public function testGetDefinedFunctionsUserCount()
    {
        $definedFunctionsUserCount = $this->profilerCollector->getDefinedFunctionsUserCount();
        $this->assertGreaterThan(1, $definedFunctionsUserCount);
    }

    public function testGetName()
    {
        $name = $this->profilerCollector->getName();
        $this->assertEquals('profiler', $name);
    }
}
