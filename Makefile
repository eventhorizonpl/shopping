CSS = ./web/bundles/eventhorizonshopping/css/
JS = ./web/bundles/eventhorizonshopping/js/
LESS = ./web/bundles/eventhorizonshopping/less/shopping.less

JS_01 = ${JS}/jquery.js
JS_10 = ${JS}/affix.js
JS_11 = ${JS}/alert.js
JS_12 = ${JS}/button.js
JS_13 = ${JS}/carousel.js
JS_14 = ${JS}/collapse.js
JS_15 = ${JS}/dropdown.js
JS_16 = ${JS}/modal.js
JS_17 = ${JS}/popover.js
JS_19 = ${JS}/scrollspy.js
JS_20 = ${JS}/tab.js
JS_21 = ${JS}/tooltip.js
JS_22 = ${JS}/transition.js

all: css js

css:
	lessc --compress ${LESS} > ${CSS}/main.css

js:
	cat ${JS_01} ${JS_10} ${JS_11} ${JS_12} ${JS_13} ${JS_14} ${JS_15} ${JS_16} ${JS_17} ${JS_18} ${JS_19} ${JS_20} ${JS_21} ${JS_22} > ${JS}/main.js
	java -jar ~/bin/yuicompressor.jar --type js -o '${JS}/main.min.js' ${JS}/main.js
	mv ${JS}/main.min.js ${JS}/main.js
