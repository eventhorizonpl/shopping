#!/bin/sh

php app/console doctrine:database:drop --env=test --force
php app/console doctrine:database:create --env=test
php app/console doctrine:schema:update --force --env=test
echo y | php app/console doctrine:fixtures:load --env=test

php app/console doctrine:mongodb:schema:drop
php app/console doctrine:mongodb:schema:create

if [ "$1" == "coverage" ]
then
    phpunit -c app --verbose --coverage-html ../tmp/report
else
    phpunit -c app --verbose
fi
