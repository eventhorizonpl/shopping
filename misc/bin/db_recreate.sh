#!/bin/sh

php app/console doctrine:database:drop --force
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
echo y | php app/console doctrine:fixtures:load

php app/console doctrine:mongodb:schema:drop
php app/console doctrine:mongodb:schema:create
